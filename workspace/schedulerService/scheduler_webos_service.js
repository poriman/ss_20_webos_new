// helloworld_webos_service.js
// simple service, based on low-level Palmbus API

var pkgInfo = require('./package.json');
var Service = require('webos-service');
// Register com.yourdomain.@DIR@.service, on both buses
var service = new Service(pkgInfo.name);
service.activityManager.idleTimeout = 600;

var fs = require('fs');
var http = require('http');

var contentlist = [{name : "m1.mp4", type : "video", playtime : 30.0 }, /*{name : "i1.jpg", type : "image", playtime : 5.0 },*/ 
                   {name : "m1.mp4", type : "video", playtime : 30.0 }/*, 
                   {name : "i2.jpg", type : "image", playtime : 5.0 }, 
                   {name : "m2.mp4", type : "video", playtime : 30.0 }, 
                   {name : "i3.png", type : "image", playtime : 5.0 }*/];

var lastDeploySerialNumber = 0;
var lastDeployScheduleList = "";
//var deployScheduleList = '{"retCode":"S0000","retMessage":"연동을 성공하였습니다.","deploySerialNumber":"59821","deployGroupSerialNumber":"59821","deployDateTime":"20171019213500","deployScheduleInfo":{"deployInformation":{"scheduleGroupId":"59821","equipmentGroupId":"","deployWeekNumber":"","deployVersion":"","deployUserId":"","deployDateTime":"20171019213500","deployCode":"W","multiCode":"","multiSlaveCount":"","basicScheduleId":"0"},"weekCalendar":{"basicScheduleId":"0","weekList":[{"weekNumber":"42","startDate":"20171015","sunday":"14208","monday":"14208","tuesday":"14208","wednesday":"14208","thursday":"14384","friday":"14384","saturday":"14384"},{"weekNumber":"43","startDate":"20171022","sunday":"14384","monday":"14384","tuesday":"14384","wednesday":"14384","thursday":"14384","friday":"14384","saturday":"14384"}]},"repeatCalendar":{"basicScheduleId":"0","weekList":[{"weekNumber":null,"startDate":null,"sunday":"14384","monday":"14384","tuesday":"14384","wednesday":"14384","thursday":"14384","friday":"14384","saturday":"14384"}]},"scheduleList":[{"scheduleId":"14208","name":"신현섭","startTime":"0000","endTimd":"2400","playList":[{"totalPlayTime":"20","startTime":"0000","endTime":"2400","startAction":null,"endAction":null,"splitScreenYn":"N","splitScreenId":"0","ruleActionList":[],"playlistContentsList":[{"splitScreenIndex":"0","splitContentsList":[{"sequence":"0","contentsId":"10021861","appointmentPlayTime":"20","startAction":null,"endAction":null,"playDiv":"F","campaignRuleId":"0","ruleActionList":[]}]}]}]},{"scheduleId":"14384","name":"신현섭","startTime":"0000","endTimd":"2400","playList":[{"totalPlayTime":"32","startTime":"0000","endTime":"2400","startAction":null,"endAction":null,"splitScreenYn":"N","splitScreenId":"0","ruleActionList":[],"playlistContentsList":[{"splitScreenIndex":"0","splitContentsList":[{"sequence":"0","contentsId":"10020931","appointmentPlayTime":"32","startAction":null,"endAction":null,"playDiv":"F","campaignRuleId":"0","ruleActionList":[]}]}]}]}],"contentsDetailList":[{"contentsId":"10020931","playContentsId":"10020931","uri":null,"version":"0","height":"1080","width":"1920","direction":"0","playTime":"32","type":"MO","pptCount":"0","pptIntervalTime":"0","contentsExt":"mp4","contentsFileSize":"20015039"},{"contentsId":"10021861","playContentsId":"10021861","uri":null,"version":"0","height":"1080","width":"1920","direction":"0","playTime":"20","type":"MO","pptCount":"0","pptIntervalTime":"0","contentsExt":"wmv","contentsFileSize":"22613431"}]}}';
var deployScheduleList = '{	"retCode": "S0000",	"retMessage": "연동을 성공하였습니다.",	"deploySerialNumber": "60682",	"deployGroupSerialNumber": "60682",	"deployDateTime": "20171030112522",	"deployScheduleInfo": {		"deployInformation": {			"scheduleGroupId": "60682",			"equipmentGroupId": "",			"deployWeekNumber": "",			"deployVersion": "",			"deployUserId": "",			"deployDateTime": "20171030112522",			"deployCode": "W",			"multiCode": "",			"multiSlaveCount": "",			"basicScheduleId": "0"		},		"weekCalendar": {			"basicScheduleId": "0",			"weekList": [{				"weekNumber": "46",				"startDate": "20171112",				"sunday": "14404",				"monday": "14404",				"tuesday": "14404",				"wednesday": "14404",				"thursday": "14404",				"friday": "14404",				"saturday": "14404"			},			{				"weekNumber": "47",				"startDate": "20171119",				"sunday": "14404",				"monday": "14404",				"tuesday": "14404",				"wednesday": "14404",				"thursday": "14404",				"friday": "14404",				"saturday": "14404"			}]		},		"repeatCalendar": {			"basicScheduleId": "0",			"weekList": [{				"weekNumber": null,				"startDate": null,				"sunday": "14404",				"monday": "14404",				"tuesday": "14404",				"wednesday": "14404",				"thursday": "14404",				"friday": "14404",				"saturday": "14404"			}]		},		"scheduleList": [{			"scheduleId": "14404",			"name": "신현섭",			"startTime": "0000",			"endTimd": "2400",			"playList": [{				"totalPlayTime": "342",				"startTime": "0000",				"endTime": "2400",				"startAction": null,				"endAction": null,				"splitScreenYn": "N",				"splitScreenId": "0",				"ruleActionList": [],				"playlistContentsList": [{					"splitScreenIndex": "0",					"splitContentsList": [{						"sequence": "0",						"contentsId": "10020931",						"appointmentPlayTime": "32",						"startAction": null,						"endAction": null,						"playDiv": "F",						"campaignRuleId": "0",						"ruleActionList": []					},					{						"sequence": "1",						"contentsId": "10021712",						"appointmentPlayTime": "10",						"startAction": null,						"endAction": null,						"playDiv": "F",						"campaignRuleId": "0",						"ruleActionList": []					},					{						"sequence": "2",						"contentsId": "10021972",						"appointmentPlayTime": "10",						"startAction": null,						"endAction": null,						"playDiv": "F",						"campaignRuleId": "0",						"ruleActionList": []					}]				}]			}]		}],		"contentsDetailList": [{			"contentsId": "10020931",			"playContentsId": "10020931",			"uri": null,			"version": "0",			"height": "1080",			"width": "1920",			"direction": "0",			"playTime": "32",			"type": "MO",			"pptCount": "0",			"pptIntervalTime": "0",			"contentsExt": "mp4",			"contentsFileSize": "20015039"		},		{			"contentsId": "10021712",			"playContentsId": "10021712",			"uri": null,			"version": "0",			"height": "407",			"width": "600",			"direction": "0",			"playTime": "0",			"type": "IM",			"pptCount": "0",			"pptIntervalTime": "0",			"contentsExt": "jpg",			"contentsFileSize": "30772"		},		{			"contentsId": "10021972",			"playContentsId": "10021972",			"uri": null,			"version": "0",			"height": "407",			"width": "600",			"direction": "0",			"playTime": "0",			"type": "IM",			"pptCount": "0",			"pptIntervalTime": "0",			"contentsExt": "jpg",			"contentsFileSize": "30772"		},		{			"contentsId": "10022024",			"playContentsId": "10022023",			"uri": null,			"version": "0",			"height": "1080",			"width": "1920",			"direction": "0",			"playTime": "310",			"type": "PA",			"pptCount": "0",			"pptIntervalTime": "0",			"contentsExt": "pkg",			"contentsFileSize": "2673"		}]	}}';

var currentScheduleObject;
var currentContentList;
var currentContentObject;
var currentScheduleIndex = -1;

var current = -1;

service.register("initContentIndex", function(message) {
	
	console.log("In initContentIndex callback");
	try {
		currentScheduleIndex = -1;
		
		message.respond({
			returnValue: true
		});
	}
	catch (e) {
		message.respond({
			returnValue: false,
			errorText: "null",
			errorCode: 1
		});

	}
});

service.register("hasPlayContent", function(message) {
	
	console.log("In hasPlayContent callback");
	try {
		checkSchedule(function (bRet) {
			try{
				if(bRet && bRet == true) {

					var nextCont;
					
					if(currentScheduleObject){
						
						var today = new Date();

//						var tz = (today.getTimezoneOffset() * 60000) + (9 * 3600000);
//						today.setTime(tz); // 한국 시간 설정
						
						var hh = today.getHours();
						var mm = today.getMinutes();
						
						var todayHHMM;
						
						if(hh < 10) todayHHMM = '0' + hh;
						else todayHHMM = '' + hh;
						
						if(mm < 10) todayHHMM += ('0' + mm);
						else todayHHMM += mm;

						if(currentScheduleObject.startTime <= todayHHMM && currentScheduleObject.endTimd >= todayHHMM){
							if(currentScheduleObject.playList){
								for(var idxPl in currentScheduleObject.playList){
									
									var pl = currentScheduleObject.playList[idxPl];
									if(pl.startTime <= todayHHMM && pl.endTime >= todayHHMM){
										
										if(pl.playlistContentsList){

											var contList = pl.playlistContentsList[0];
											
											if(contList && contList.splitContentsList){
												contList.splitContentsList
												
												message.respond({
													returnValue: true,
													hasContent: contList.splitContentsList.length > 0 ? true : false
												});
												
												return;

											}
										}
									}
								}
							}
						}

					}
					else {
						message.respond({
							returnValue: false,
							hasContent: false,
							errorText: "schedule is null",
							errorCode: 1
						});
						
						
						return;
					}
					
					/// 컨텐트 없음
					message.respond({
						returnValue: false,
						hasContent: false,
						errorText: "content is null",
						errorCode: 1
					});

				}
				else {
					message.respond({
						returnValue: false,
						hasContent: false,
						errorText: "schedule is null",
						errorCode: 1
					});
				}


			}
			catch (ex) {
				/// 파싱중 예외 발생
				message.respond({
					returnValue: false,
					hasContent: false,
					errorText: "out checkSchedule exception: " + ex,
					errorCode: 1
				});

			}
			
			
		});
	}
	catch (e) {
		/// 파싱중 예외 발생
		message.respond({
			returnValue: false,
			hasContent: false,
			errorText: "in checkSchedule exception: " + e,
			errorCode: 1
		});
	}
	

});

service.register("nextContent", function(message) {
	
	var sentMessage = false;
	
	console.log("In nextContent callback");
	try {
		checkSchedule(function (bRet) {
			try{
				if(bRet && bRet == true) {

					var nextCont;
					
					if(currentScheduleObject){
						
						var today = new Date();

//						var tz = (today.getTimezoneOffset() * 60000) + (9 * 3600000);
//						today.setTime(tz); // 한국 시간 설정
						
						var hh = today.getHours();
						var mm = today.getMinutes();
						
						var todayHHMM;
						
						if(hh < 10) todayHHMM = '0' + hh;
						else todayHHMM = '' + hh;
						
						if(mm < 10) todayHHMM += ('0' + mm);
						else todayHHMM += mm;

						if(currentScheduleObject.startTime <= todayHHMM && currentScheduleObject.endTimd >= todayHHMM){
							if(currentScheduleObject.playList){
								for(var idxPl in currentScheduleObject.playList){
									
									var pl = currentScheduleObject.playList[idxPl];
									if(pl.startTime <= todayHHMM && pl.endTime >= todayHHMM){
										
										if(pl.playlistContentsList){

											var contList = pl.playlistContentsList[0];
											
											if(contList && contList.splitContentsList){
												var nextCont = contList.splitContentsList[++currentScheduleIndex];
												if(!nextCont)
													nextCont = contList.splitContentsList[currentScheduleIndex = 0];
												
												var nextCont2 = contList.splitContentsList[currentScheduleIndex + 1];
												if(!nextCont2)
													nextCont2 = contList.splitContentsList[0];
												
												var c1 = null;
												var c2 = null;
												
												for(var idxContObj in currentContentList) {
													var contObj = currentContentList[idxContObj];
													
													if(contObj.contentsId == nextCont.contentsId){
														
														contObj.playTime = nextCont.appointmentPlayTime;
														c1 = contObj;
													}
													
													if(contObj.contentsId == nextCont2.contentsId){
														
														contObj.playTime = nextCont2.appointmentPlayTime;
														c2 = contObj;
													}
													
													if(c1 && c2) break;
												}
												
												message.respond({
													returnValue: true,
													content: currentContentObject = c1,
													nextContent: c2,
													schedule : currentScheduleObject
												});
												
												sentMessage = true;

												return ;

											}
										}
									}
								}
							}
						}

					}
					else {
						message.respond({
							returnValue: false,
							errorText: "schedule is null",
							errorCode: 1
						});
						sentMessage = true;
						
						return;
					}
					
					/// 컨텐트 없음
					message.respond({
						returnValue: false,
						errorText: "content is null",
						errorCode: 1
					});
					sentMessage = true;

				}
				else {
					message.respond({
						returnValue: false,
						errorText: "schedule is null",
						errorCode: 1
					});
					sentMessage = true;
				}


			}
			catch (ex) {
				/// 파싱중 예외 발생
				message.respond({
					returnValue: false,
					errorText: "out checkSchedule exception: " + ex,
					errorCode: 1
				});
				sentMessage = true;
			}

			if(!sentMessage) {
				message.respond({
					returnValue: false,
					errorText: "sentMessage is null",
					errorCode: 1
				});
			}
		});
	}
	catch (e) {
		/// 파싱중 예외 발생
		message.respond({
			returnValue: false,
			errorText: "in checkSchedule exception: " + e,
			errorCode: 1
		});
	}
	

});

service.register("getPlaylist", function(message) {
	
	console.log("In getPlaylist callback");
	try {
		checkSchedule(function (bRet) {
			try{
				if(bRet && bRet == true) {

					var nextCont;
					
					if(currentScheduleObject){
						
						var today = new Date();

//						var tz = (today.getTimezoneOffset() * 60000) + (9 * 3600000);
//						today.setTime(tz); // 한국 시간 설정
						
						var hh = today.getHours();
						var mm = today.getMinutes();
						
						var todayHHMM;
						
						if(hh < 10) todayHHMM = '0' + hh;
						else todayHHMM = '' + hh;
						
						if(mm < 10) todayHHMM += ('0' + mm);
						else todayHHMM += mm;

						if(currentScheduleObject.startTime <= todayHHMM && currentScheduleObject.endTimd >= todayHHMM){
							if(currentScheduleObject.playList){
								for(var idxPl in currentScheduleObject.playList){
									
									var pl = currentScheduleObject.playList[idxPl];
									if(pl.startTime <= todayHHMM && pl.endTime >= todayHHMM){
										
										if(pl.playlistContentsList){

											var contList = pl.playlistContentsList[0];
											
											var playlist = [];
											if(contList && contList.splitContentsList){
												
												for(var idxPlaylist = 0 ; idxPlaylist < contList.splitContentsList.length ; ++idxPlaylist) {
													var contPl = contList.splitContentsList[idxPlaylist];
													
													if(contPl) {
														for(var idxContObj in currentContentList) {
															var contObj = currentContentList[idxContObj];
															
															if(contObj.contentsId == contPl.contentsId){
																
																contObj.playTime = contPl.appointmentPlayTime;
																
																playlist.push(contObj);
																
																break;
															}
														}
													}
													
												}
											
												message.respond({
													returnValue: true,
													playlist: playlist,
													schedule : currentScheduleObject
												});

											}
											else {
												message.respond({
													returnValue: false,
													errorText: "schedule is null",
													errorCode: 1
												});
											}
										}
									}
								}
							}
						}

					}
					else {
						message.respond({
							returnValue: false,
							errorText: "schedule is null",
							errorCode: 1
						});
						
						
						return;
					}
					
					/// 컨텐트 없음
					message.respond({
						returnValue: false,
						errorText: "content is null",
						errorCode: 1
					});

				}
				else {
					message.respond({
						returnValue: false,
						errorText: "schedule is null",
						errorCode: 1
					});
				}


			}
			catch (ex) {
				/// 파싱중 예외 발생
				message.respond({
					returnValue: false,
					errorText: "out checkSchedule exception: " + ex,
					errorCode: 1
				});

			}
			
			
		});
	}
	catch (e) {
		/// 파싱중 예외 발생
		message.respond({
			returnValue: false,
			errorText: "in checkSchedule exception: " + e,
			errorCode: 1
		});
	}
	

});

/*
 * 파일 갱신 상태를 조회한다.
 */
function readScheduleFile(_callback) {
	try {
	   fs.readFile('/media/internal/ssp/deploySchedule.txt', function (err, messageData) {
	        
		   if (err) {
	       		lastDeployScheduleList = ''; /*deployScheduleList;*/ // TEST 디버깅을 위해
	       		// TO-DO : Retry 로직이 넣어야할듯 (파일 읽기쓰기가  다른 서비스와 중복될 가능성으로 실패할 경우를 대비)
	       }
	       else {
	        	lastDeployScheduleList = messageData.toString();
	       }
		   
		   _callback(lastDeployScheduleList);
	   });

	}
	catch (e) {
		_callback();
	}
	
}

/*
 * 스케줄을 분석한다.
 */
function checkSchedule(_callbackResult){
	try {
		readScheduleFile(function(filedata) { 
			try {
				if(filedata){
					var objSchedule = JSON.parse(filedata);

					var serialNumber = parseInt(objSchedule.deploySerialNumber);
					
					if(serialNumber != lastDeploySerialNumber) {
						// 현재 재생중인 재생목록 위치 초기화
						currentScheduleIndex = -1;
						lastDeploySerialNumber = serialNumber;
					}

					var today = new Date();
					
//					var tz = (today.getTimezoneOffset() * 60000) + (9 * 3600000);
//					today.setTime(tz); // 한국 시간 설정

					var todayYYMMDD = today.toISOString().substring(0, 10).replace("-", "").replace("-", "");	

					var currentScheduleId;
					
					var hasWeekSched = false;
					
					if(objSchedule.deployScheduleInfo.weekCalendar && objSchedule.deployScheduleInfo.weekCalendar.weekList){
						
						for(var idxWeek in objSchedule.deployScheduleInfo.weekCalendar.weekList){
							var week = objSchedule.deployScheduleInfo.weekCalendar.weekList[idxWeek];
							
							if(todayYYMMDD >= week.startDate){
								switch (today.getDay()){
								case 0: // 일
									currentScheduleId = week.sunday;
									hasWeekSched = true;
									break;
								case 1: // 월
									currentScheduleId = week.monday;
									hasWeekSched = true;
									break;
								case 2: // 화
									currentScheduleId = week.tuesday;
									hasWeekSched = true;
									break;
								case 3: // 수
									currentScheduleId = week.wednesday;
									hasWeekSched = true;
									break;
								case 4: // 목
									currentScheduleId = week.thursday;
									hasWeekSched = true;
									break;
								case 5: // 금
									currentScheduleId = week.friday;
									hasWeekSched = true;
									break;
								case 6: // 토
									currentScheduleId = week.saturday;
									hasWeekSched = true;
									break;
								}
							}
								
							if(currentScheduleId) break;
						}
					}

					if(hasWeekSched == false && !currentScheduleId) {
						if(objSchedule.deployScheduleInfo.repeatCalendar && objSchedule.deployScheduleInfo.repeatCalendar.weekList){
							
							for(var idxWeek in objSchedule.deployScheduleInfo.repeatCalendar.weekList){
								
								var week = objSchedule.deployScheduleInfo.repeatCalendar.weekList[idxWeek];
								if(todayYYMMDD > week.startDate){
									switch (today.getDay()){
									case 0: // 일
										currentScheduleId = week.sunday;
										break;
									case 1: // 월
										currentScheduleId = week.monday;
										break;
									case 2: // 화
										currentScheduleId = week.tuesday;
										break;
									case 3: // 수
										currentScheduleId = week.wednesday;
										break;
									case 4: // 목
										currentScheduleId = week.thursday;
										break;
									case 5: // 금
										currentScheduleId = week.friday;
										break;
									case 6: // 토
										currentScheduleId = week.saturday;
										break;
									}
								}
									
								if(currentScheduleId) break;
							}
						}
					}
						
					if(currentScheduleId && objSchedule.deployScheduleInfo.scheduleList){
						/// 스케줄이 존재함
						
						for(var idxSchedule in objSchedule.deployScheduleInfo.scheduleList){
							var schedule = objSchedule.deployScheduleInfo.scheduleList[idxSchedule];
							if(schedule && schedule.scheduleId == currentScheduleId){
								
								currentScheduleObject = schedule;
								currentContentList = objSchedule.deployScheduleInfo.contentsDetailList;
								
								console.log(currentScheduleObject);
								break;
							}
						}
						
					}	
					else{
						///	스케줄이 존재하지 않음
						_callbackResult(false);
						return;
					}

				}
				else {
					///	스케줄이 존재하지 않음
					_callbackResult(false);
					return;
				}
				
				if(currentScheduleId && currentScheduleObject)
					_callbackResult(true);
				else
					_callbackResult(false);
				
				return;
			} catch(_Ex) {
				console.error(_Ex);
				_callbackResult(false);
			}
		});
	} catch (e){ 
		console.log(e);
		_callbackResult(false);
	}
}


var pkgInfo = require('./package.json');
var Service = require('webos-service');
var service = new Service(pkgInfo.name);
service.activityManager.idleTimeout = 600;

var Define = require("./common/Define.js");
var HttpRequest = require("./common/HttpRequest.js");

var ScheduleManager = require('./schedule/ScheduleManager.js');

var FileUtil = require('./common/FileUtil.js');

var fs = require("fs");

var SCHEDULE_INTERVAL_ID = 0;
var SCHEDULE_INTERVAL_DELAY = 1000 * 5;

function getRetMsg() {
	var retMsg = {
		returnValue : true,
		retCode : "",
		retMessage : "",
		data : null,
		errorText : "",
		errorCode : ""
	};
	
	return retMsg;
}

// SERVER_EXIST_EQUIPMENT
service.register("existEquipment", function(_message) {
	var retMsg = getRetMsg();

	try {
		var postData = { equipmentMacAddress : _message.payload.macAddress };
		
		HttpRequest.post(Define.SERVER_EXIST_EQUIPMENT, postData, function(_successData) {
			if(_successData) {
				retMsg.returnValue = true;
				retMsg.retCode = _successData.retCode;
				retMsg.retMessage = _successData.retMessage;
			} else {
				retMsg.returnValue = true;
				retMsg.errorText = "SERVER_ERROR";
				retMsg.errorCode = -1;
			}
			
			_message.respond(retMsg);
		}, function(_err) {
			retMsg.returnValue = false;
			retMsg.errorText = _err.message;
			retMsg.errorCode =  _err.code;
			
			_message.respond(retMsg);
		});
	} catch(_Ex) {
		console.error(_Ex);
		
		retMsg.returnValue = false;
		
		if(_Ex) {
			retMsg.errorText = _Ex.message ? _Ex.message : "";
			retMsg.errorCode =  _Ex.code ? _Ex.code : "";
		} else {
			retMsg.errorText = "SERVER_ERROR";
			retMsg.errorCode = -1;
		}
		
		_message.respond(retMsg);
	}
});

//SERVER_SEARCH_EQUIPMENT_INFO
service.register("searchEquipmentInfo", function(_message) {
	var retMsg = getRetMsg();
	
	try {
		var postData = { token : _message.payload.token };
		
		HttpRequest.post(Define.SERVER_SEARCH_EQUIPMENT_INFO, postData, function(_successData) {
			try {
				if(_successData) {
					retMsg.returnValue = true;
					retMsg.retCode = _successData.retCode;
					retMsg.retMessage = _successData.retMessage;
					retMsg.data = {
						equipmentSerialNo : _successData.equipmentSerialNo,
						customerSerialNo : _successData.customerSerialNo,
						shopSerialNo : _successData.shopSerialNo,
						multiGroupSerialNo : _successData.multiGroupSerialNo,
						equipmentName : _successData.equipmentName,
						equipmentDirection : _successData.equipmentDirection,
						equipmentTypeCode : _successData.equipmentTypeCode,
						customerName : _successData.customerName,
						shopName : _successData.shopName,
						multiGroupType : _successData.multiGroupType,
						multiGroupName : _successData.multiGroupName
					}
				} else {
					retMsg.returnValue = true;
					retMsg.errorText = "SERVER_ERROR";
					retMsg.errorCode = -1;
				}
			} catch(_Ex) {
				console.error(_Ex);
				
				retMsg.returnValue = true;
				
				if(_Ex) {
					retMsg.errorText = _Ex.message ? _Ex.message : "";
					retMsg.errorCode =  _Ex.code ? _Ex.code : "";
				} else {
					retMsg.errorText = "SERVER_ERROR";
					retMsg.errorCode = -1;
				}
			}
			
			_message.respond(retMsg);
		}, function(_err) {
			retMsg.returnValue = false;
			retMsg.errorText = _err.message;
			retMsg.errorCode =  _err.code;
			
			_message.respond(retMsg);
		});
	} catch(_Ex) {
		console.error(_Ex);
		
		retMsg.returnValue = false;
		
		if(_Ex) {
			retMsg.errorText = _Ex.message ? _Ex.message : "";
			retMsg.errorCode =  _Ex.code ? _Ex.code : "";
		} else {
			retMsg.errorText = "SERVER_ERROR";
			retMsg.errorCode = -1;
		}
		
		_message.respond(retMsg);
	}
});

// SERVER_REGIST_EQUIPMENT
service.register("registerEquipment", function(_message) {
	var retMsg = getRetMsg();
	
	try {
		var postData = {
				openApprovalNo : _message.payload.openApprovalNo,
		    	equipmentModelName : _message.payload.equipmentModelName,
		    	equipmentScreenDirection : _message.payload.equipmentScreenDirection,
		    	equipmentMacAddress : _message.payload.equipmentMacAddress,
		    	equipmentSerialNumber : _message.payload.equipmentSerialNumber,
		    	equipmentSupportResolution : _message.payload.equipmentSupportResolution,
		    	firmwareVersion : _message.payload.firmwareVersion,
		    	equipmentName : _message.payload.equipmentName,
		    	equipmentVolumeStatus : _message.payload.equipmentVolumeStatus,
		    	screenEquipmentVolumeStatus : _message.payload.screenEquipmentVolumeStatus,
		    	equipmentBeaconStatus : _message.payload.equipmentBeaconStatus,
		    	equipmentConnectionType : _message.payload.equipmentConnectionType,
		    	appList : _message.payload.appList
			};
			
			HttpRequest.post(Define.SERVER_REGIST_EQUIPMENT, postData, function(_successData) {
				if(_successData) {
					retMsg.returnValue = true;
					retMsg.retCode = _successData.retCode;
					retMsg.retMessage = _successData.retMessage;
				} else {
					retMsg.returnValue = true;
					retMsg.errorText = "SERVER_ERROR";
					retMsg.errorCode = -1;
				}
				
				_message.respond(retMsg);
			}, function(_err) {
				retMsg.returnValue = false;
				retMsg.errorText = _err.message;
				retMsg.errorCode =  _err.code;
				
				_message.respond(retMsg);
			});
	} catch(_Ex) {
		console.error(_Ex);
		
		retMsg.returnValue = false;
		
		if(_Ex) {
			retMsg.errorText = _Ex.message ? _Ex.message : "";
			retMsg.errorCode =  _Ex.code ? _Ex.code : "";
		} else {
			retMsg.errorText = "SERVER_ERROR";
			retMsg.errorCode = -1;
		}
		
		_message.respond(retMsg);
	}
});

//SERVER_CHANGE_EQUIPMENT_NAME
service.register("changeEquipmentInfo", function(_message) {
	var retMsg = getRetMsg();
	
	try {
		var postData = {
			token : _message.payload.token,
			equipmentName : _message.payload.equipmentName,
			equipmentScreenDirection : _message.payload.equipmentScreenDirection
		};
		
		HttpRequest.post(Define.SERVER_CHANGE_EQUIPMENT_NAME, postData, function(_successData) {
			if(_successData) {
				retMsg.returnValue = true;
				retMsg.retCode = _successData.retCode;
				retMsg.retMessage = _successData.retMessage;
			} else {
				retMsg.returnValue = true;
				retMsg.errorText = "SERVER_ERROR";
				retMsg.errorCode = -1;
			}
			
			_message.respond(retMsg);
		}, function(_err) {
			retMsg.returnValue = false;
			retMsg.errorText = _err.message;
			retMsg.errorCode =  _err.code;
			
			_message.respond(retMsg);
		});
	} catch(_Ex) {
		console.error(_Ex);
		
		retMsg.returnValue = false;
		if(_Ex) {
			retMsg.errorText = _Ex.message ? _Ex.message : "";
			retMsg.errorCode =  _Ex.code ? _Ex.code : "";
		} else {
			retMsg.errorText = "SERVER_ERROR";
			retMsg.errorCode = -1;
		}
		
		_message.respond(retMsg);
	}
});

//SERVER_SHOP_EQUIPMENT
service.register("shopEquipment", function(_message) {
	var retMsg = getRetMsg();
	
	try {
		var postData = { openApprovalNo: _message.payload.equipmentAuthCode, equipmentMacAddress : _message.payload.macAddress };
		
		HttpRequest.post(Define.SERVER_SHOP_EQUIPMENT, postData, function(_successData) {
			if(_successData) {
				retMsg.returnValue = true;
				retMsg.retCode = _successData.retCode;
				retMsg.retMessage = _successData.retMessage;
				retMsg.data = {
					customerName : _successData.customerName,
					shopName : _successData.shopName,
					equipmentName : _successData.equipmentName
				};
			} else {
				retMsg.returnValue = true;
				retMsg.errorText = "SERVER_ERROR";
				retMsg.errorCode = -1;
			}
			
			_message.respond(retMsg);
		}, function(_err) {
			retMsg.returnValue = false;
			retMsg.errorText = _err.message;
			retMsg.errorCode =  _err.code;
			
			_message.respond(retMsg);
		});
	} catch(_Ex) {
		console.error(_Ex);
		
		retMsg.returnValue = false;
		
		if(_Ex) {
			retMsg.errorText = _Ex.message ? _Ex.message : "";
			retMsg.errorCode =  _Ex.code ? _Ex.code : "";
		} else {
			retMsg.errorText = "SERVER_ERROR";
			retMsg.errorCode = -1;
		}
		
		_message.respond(retMsg);
	}
});

//SERVER_CREATE_TOKEN
service.register("createToken", function(_message) {
	var retMsg = getRetMsg();
	
	try {
		var postData = { equipmentMacAddress: _message.payload.macAddress };
		
		HttpRequest.post(Define.SERVER_CREATE_TOKEN, postData, function(_successData) {
			if(_successData) {
				retMsg.returnValue = true;
				retMsg.retCode = _successData.retCode;
				retMsg.retMessage = _successData.retMessage;
				retMsg.data = {
					token : _successData.token,
					expireDateTime: _successData.expireDateTime
				};
			} else {
				retMsg.returnValue = true;
				retMsg.errorText = "SERVER_ERROR";
				retMsg.errorCode = -1;
			}
			
			_message.respond(retMsg);
		}, function(_err) {
			retMsg.returnValue = false;
			retMsg.errorText = _err.message;
			retMsg.errorCode =  _err.code;
			
			_message.respond(retMsg);
		});
	} catch(_Ex) {
		console.error(_Ex);
		
		retMsg.returnValue = false;
		if(_Ex) {
			retMsg.errorText = _Ex.message ? _Ex.message : "";
			retMsg.errorCode =  _Ex.code ? _Ex.code : "";
		} else {
			retMsg.errorText = "SERVER_ERROR";
			retMsg.errorCode = -1;
		}
		
		_message.respond(retMsg);
	}
});

//SERVER_APPROVAL_EQUIPMENT
service.register("approvalEquipment", function(_message) {
	var retMsg = getRetMsg();
	
	try {
		var postData = { token: _message.payload.token };
		
		HttpRequest.post(Define.SERVER_APPROVAL_EQUIPMENT, postData, function(_successData) {
			if(_successData) {
				retMsg.returnValue = true;
				retMsg.retCode = _successData.retCode;
				retMsg.retMessage = _successData.retMessage;
				retMsg.data = {
					equipmentApprovalYn : _successData.equipmentApprovalYn
				};
			} else {
				retMsg.returnValue = true;
				retMsg.errorText = "SERVER_ERROR";
				retMsg.errorCode = -1;
			}
			
			_message.respond(retMsg);
		}, function(_err) {
			retMsg.returnValue = false;
			retMsg.errorText = _err.message;
			retMsg.errorCode =  _err.code;
			
			_message.respond(retMsg);
		});
	} catch(_Ex) {
		console.error(_Ex);
		
		retMsg.returnValue = false;
		if(_Ex) {
			retMsg.errorText = _Ex.message ? _Ex.message : "";
			retMsg.errorCode =  _Ex.code ? _Ex.code : "";
		} else {
			retMsg.errorText = "SERVER_ERROR";
			retMsg.errorCode = -1;
		}
		
		_message.respond(retMsg);
	}
});

//SERVER_STATUS_REPORT_EQUIPMENT
service.register("equipmentStatusResult", function(_message) {
	var retMsg = getRetMsg();
	
	try {
		var postData = _message.payload;
		
		HttpRequest.post(Define.SERVER_STATUS_REPORT_EQUIPMENT, postData, function(_successData) {
			if(_successData) {
				retMsg.returnValue = true;
				retMsg.retCode = _successData.retCode;
				retMsg.retMessage = _successData.retMessage;
			} else {
				retMsg.returnValue = true;
				retMsg.errorText = "SERVER_ERROR";
				retMsg.errorCode = -1;
			}
			
			_message.respond(retMsg);
		}, function(_err) {
			retMsg.returnValue = false;
			retMsg.errorText = _err.message;
			retMsg.errorCode =  _err.code;
			
			_message.respond(retMsg);
		});
	} catch(_Ex) {
		console.error(_Ex);
		
		retMsg.returnValue = false;
		if(_Ex) {
			retMsg.errorText = _Ex.message ? _Ex.message : "";
			retMsg.errorCode =  _Ex.code ? _Ex.code : "";
		} else {
			retMsg.errorText = "SERVER_ERROR";
			retMsg.errorCode = -1;
		}
		
		_message.respond(retMsg);
	}
});

//SERVER_LOG_EQUIPMENT
service.register("logEquipment", function(_message) {
	var retMsg = getRetMsg();
	
	try {
		var postData = _message.payload;
		
		HttpRequest.post(Define.SERVER_LOG_EQUIPMENT, postData, function(_successData) {
			if(_successData) {
				retMsg.returnValue = true;
				retMsg.retCode = _successData.retCode;
				retMsg.retMessage = _successData.retMessage;
			} else {
				retMsg.returnValue = true;
				retMsg.errorText = "SERVER_ERROR";
				retMsg.errorCode = -1;
			}
			
			_message.respond(retMsg);
		}, function(_err) {
			retMsg.returnValue = false;
			retMsg.errorText = _err.message;
			retMsg.errorCode =  _err.code;
			
			_message.respond(retMsg);
		});
	} catch(_Ex) {
		console.error(_Ex);
		
		retMsg.returnValue = false;
		if(_Ex) {
			retMsg.errorText = _Ex.message ? _Ex.message : "";
			retMsg.errorCode =  _Ex.code ? _Ex.code : "";
		} else {
			retMsg.errorText = "SERVER_ERROR";
			retMsg.errorCode = -1;
		}
		
		_message.respond(retMsg);
	}
});


//SERVER_MIRRORING_IMAGE_UPLOAD
service.register("mirrorEquipment", function(_message) {
	var retMsg = getRetMsg();

	try {
		var mirrorFile = _message.payload.mirrorFile;
		var mirroringCount = _message.payload.mirroringCount;
		
		var data = mirrorFile.replace(/^data:image\/\w+;base64,/, "");
		var buf = new Buffer(mirrorFile, 'base64');
		
		FileUtil.makeDirectory(Define.DEFAULT_STORAGE_PATH);
		FileUtil.makeDirectory(Define.DEFAULT_MIRRORING_DIR_PATH);
		
		var fileName = Define.DEFAULT_MIRRORING_DIR_PATH + Define.DEFAULT_MIRRORING_FILE_NAME.replace(".", mirroringCount + ".");
		fs.writeFileSync(fileName, buf);
		
		var postData = {
			token: _message.payload.token,
			equipmentMirrorStopYn: _message.payload.equipmentMirrorStopYn
		}
		
		HttpRequest.sendFile(Define.SERVER_MIRRORING_IMAGE_UPLOAD, postData, fileName, function(_successData) {
			if(_successData) {
				retMsg.returnValue = true;
				retMsg.retCode = _successData.retCode;
				retMsg.retMessage = _successData.retMessage;
			} else {
				retMsg.returnValue = true;
				retMsg.errorText = "SERVER_ERROR";
				retMsg.errorCode = -1;
			}
			
			_message.respond(retMsg);
		}, function(_err) {
			retMsg.returnValue = false;
			retMsg.errorText = _err.message;
			retMsg.errorCode =  _err.code;
			
			_message.respond(retMsg);
		});
	} catch(_Ex) {
		console.error(_Ex);
		
		retMsg.returnValue = false;
		
		if(_Ex) {
			retMsg.errorText = _Ex.message ? _Ex.message : "";
			retMsg.errorCode =  _Ex.code ? _Ex.code : "";
		} else {
			retMsg.errorText = "SERVER_ERROR";
			retMsg.errorCode = -1;
		}
		
		_message.respond(retMsg);
	}
});

//SERVER_SCHEDULE_UPDATE
service.register("statusSchedule", function(_message) {
	var retMsg = getRetMsg();
	
	try {
		if(ScheduleManager.isBusy == true) {
			ScheduleManager.downloadCallback = function (_data) {
				var messageData = { returnValue : true, retCode: 'DOWNLOAD', data: _data };
				_message.respond(messageData);
			};
			ScheduleManager.downloadFinishCallback = function (_status, _err) {
				var messageData = { returnValue : true, retCode: 'DOWNLOAD_FINISH', data: _status, error: _err };
				_message.respond(messageData);
			};
			
			retMsg.returnValue = true;
			retMsg.retCode = "SCHEDULE_BUSY";
			
			_message.respond(retMsg);
		}
		else {
			var postData = { token: _message.payload.token };
			
			HttpRequest.post(Define.SERVER_SCHEDULE_UPDATE, postData, function(_successData) {
				try {
					if(_successData) {
						retMsg.returnValue = true;
						retMsg.retCode = _successData.retCode;
						retMsg.retMessage = _successData.retMessage;
						retMsg.data = {
							scheduleDeployYn : _successData.scheduleDeployYn,
							scheduleDeployStopYn : _successData.scheduleDeployStopYn,
							scheduleGroupSerialNumber : _successData.scheduleGroupSerialNumber,
							liveTextDeployYn : _successData.liveTextDeployYn,
							liveTextDeployStopYn : _successData.liveTextDeployStopYn
						};
						
						if(_successData.retCode == "S0000") {
							ScheduleManager.downloadCallback = function (_data) {
								var messageData = { returnValue : true, retCode: 'DOWNLOAD', data: _data };
								_message.respond(messageData);
							};
							ScheduleManager.downloadFinishCallback = function (_status, _err) {
								var messageData = { returnValue : true, retCode: 'DOWNLOAD_FINISH', data: _status, error: _err };
								_message.respond(messageData);
							};

							ScheduleManager.startStatusDeploy(_message.payload.token, _message.payload.totalFreeExternal, retMsg.data);
						}
					} else {
						retMsg.returnValue = true;
						retMsg.errorText = "SERVER_ERROR";
						retMsg.errorCode = -1;
					}
				} catch(_Ex) {
					console.error(_Ex);
					
					retMsg.returnValue = false;
					if(_Ex) {
						retMsg.errorText = _Ex.message ? _Ex.message : "";
						retMsg.errorCode =  _Ex.code ? _Ex.code : "";
					} else {
						retMsg.errorText = "SERVER_ERROR";
						retMsg.errorCode = -1;
					}
				}
				
				_message.respond(retMsg);
			}, function(_err) {
				retMsg.returnValue = false;
				retMsg.errorText = _err.message;
				retMsg.errorCode =  _err.code;
				
				_message.respond(retMsg);
			});

		}
	} catch(_Ex) {
		console.error(_Ex);
		
		retMsg.returnValue = false;
		if(_Ex) {
			retMsg.errorText = _Ex.message ? _Ex.message : "";
			retMsg.errorCode =  _Ex.code ? _Ex.code : "";
		} else {
			retMsg.errorText = "SERVER_ERROR";
			retMsg.errorCode = -1;
		}
		
		_message.respond(retMsg);
	}
});

//SERVER_STATUS_EQUIPMENT
service.register("statusEquipment", function(_message) {
	var retMsg = getRetMsg();
	
	try {
		var postData = { token: _message.payload.token, equipmentConnectionType: _message.payload.equipmentConnectionType, equipmentProcessAppList: _message.payload.equipmentProcessAppList };
		
		if(_message.payload.playScheduleSerialNo) {
			postData.playScheduleSerialNo = _message.payload.playScheduleSerialNo;
		}
		
		if(_message.payload.playContentsYn) {
			postData.playContentsYn = _message.payload.playContentsYn;
		}
		
		HttpRequest.post(Define.SERVER_STATUS_EQUIPMENT, postData, function(_successData) {
			try {
				if(_successData) {
					retMsg.returnValue = true;
					retMsg.retCode = _successData.retCode;
					retMsg.retMessage = _successData.retMessage;
					retMsg.data = {
						equipmentScreenStatus : _successData.equipmentScreenStatus,
						equipmentScreenDirection : _successData.equipmentScreenDirection,
						equipmentRestartStatus : _successData.equipmentRestartStatus,
						equipmentVolumeStatus : _successData.equipmentVolumeStatus,
						screenEquipmentVolumeStatus : _successData.screenEquipmentVolumeStatus,
						equipmentWifiStatus : _successData.equipmentWifiStatus,
						equipmentWifiSsid : _successData.equipmentWifiSsid,
						equipmentWifiSsidPassword : _successData.equipmentWifiSsidPassword,
						equipmentWifiConnectionUrl : _successData.equipmentWifiConnectionUrl,
						equipmentBeaconStatus : _successData.equipmentBeaconStatus,
						equipmentMirrorUseYn : _successData.equipmentMirrorUseYn,
						equipmentUseYn : _successData.equipmentUseYn,
						equipmentHardwareCollectStatus : _successData.equipmentHardwareCollectStatus,
						equipmentScreenOutputStatus : _successData.equipmentScreenOutputStatus,
						equipmentWifiType : _successData.equipmentWifiType,
						equipmentName : _successData.equipmentName,
						equipmentRealTimeStatus : _successData.equipmentRealTimeStatus,
						screenPreventInputChangeStatus : _successData.screenPreventInputChangeStatus
					};
				} else {
					retMsg.returnValue = true;
					retMsg.errorText = "SERVER_ERROR";
					retMsg.errorCode = -1;
				}
			} catch(_Ex) {
				console.error(_Ex);
				
				retMsg.returnValue = false;
				if(_Ex) {
					retMsg.errorText = _Ex.message ? _Ex.message : "";
					retMsg.errorCode =  _Ex.code ? _Ex.code : "";
				} else {
					retMsg.errorText = "SERVER_ERROR";
					retMsg.errorCode = -1;
				}
			}
			
			_message.respond(retMsg);
		}, function(_err) {
			retMsg.returnValue = false;
			retMsg.errorText = _err.message;
			retMsg.errorCode =  _err.code;
			
			_message.respond(retMsg);
		});
	} catch(_Ex) {
		console.error(_Ex);
		
		retMsg.returnValue = false;
		if(_Ex) {
			retMsg.errorText = _Ex.message ? _Ex.message : "";
			retMsg.errorCode =  _Ex.code ? _Ex.code : "";
		} else {
			retMsg.errorText = "SERVER_ERROR";
			retMsg.errorCode = -1;
		}
		
		_message.respond(retMsg);
	}
});

// REMOVE SCHEDULE FILE
service.register("removceScheduleFile", function(_message) {
	var retMsg = getRetMsg();
	
	try {
		//임시 스케쥴 파일이 있는 경우
		if (FileUtil.isFileExist(Define.DEPLOY_SCHEDULE_FILE_TMP)) {
			FileUtil.deleteFile(Define.DEPLOY_SCHEDULE_FILE_TMP);
		}
		
		//이미 deploySchedule 파일이 존재할 경우 기존 파일을 지운후
		if (FileUtil.isFileExist(Define.DEPLOY_SCHEDULE_FILE)) {
			FileUtil.deleteFile(Define.DEPLOY_SCHEDULE_FILE);
		}
		
		retMsg.returnValue = true;
		_message.respond(retMsg);
	} catch(_Ex) {
		console.error(_Ex);
		
		retMsg.returnValue = false;
		if(_Ex) {
			retMsg.errorText = _Ex.message ? _Ex.message : "";
			retMsg.errorCode =  _Ex.code ? _Ex.code : "";
		} else {
			retMsg.errorText = "SERVER_ERROR";
			retMsg.errorCode = -1;
		}
		
		_message.respond(retMsg);
	}
});

var Define = {
	SVC_DOMAIN : 							"223.39.118.83"
//	, SVC_DOMAIN : 							"api2.t-smartsignage.com"
	
// ===== START NETWORK DEFINE =====
	, SERVER_EXIST_EQUIPMENT :				"/mgmt/equipment/existEquipment"
	, SERVER_SHOP_EQUIPMENT :				"/mgmt/equipment/shopEquipment" 		//매장명 및 장치명 조회
	, SERVER_REGIST_EQUIPMENT :				"/mgmt/equipment/registEquipment"
	, SERVER_AUTHEN_KEY :					"/mgmt/equipment/createAuthenKey"
	, SERVER_CREATE_TOKEN :					"/mgmt/equipment/createToken"			//토큰 요청
	, SERVER_MIRRORING_IMAGE_UPLOAD :		"/mgmt/monitor/mirrorEquipment"			//미러링 Upload
	, SERVER_STATUS_EQUIPMENT :				"/mgmt/pulling/statusEquipment"			// Message pull..
	, SERVER_STATUS_REPORT_EQUIPMENT :		"/mgmt/pulling/equipmentStatusResult"	// 결과전송
	, SERVER_LOGFILE_UPLOAD :				"/mgmt/monitor/logRealTimeEquipment"	//로그 Upload

	, SERVER_SCHEDULE_UPDATE :				"/mgmt/pulling/statusSchedule"			// STB 스케줄 상태 조회 Pulling
	, SERVER_LOG_EQUIPMENT :				"/mgmt/monitor/logEquipment"			//로그 전송
	, SERVER_CHANGE_EQUIPMENT_NAME :		"/mgmt/equipment/changeEquipmentInfo"	// 장치정보 변경
	, SERVER_APPROVAL_EQUIPMENT :			"/mgmt/equipment/approvalEquipment"		// 장치 승인 여부 조회

	, SERVER_MONITORING_EQUIPMENT :			"/intelligence/sniffing/logGather"		//wifi모니터링 로그
	, SERVER_SEARCH_EQUIPMENT_INFO :		"/mgmt/equipment/searchEquipmentInfo"	//STB 장치 정보 조회
	, SERVER_SEARCH_EQUIPMENT_CONNECTION :	"/mgmt/equipment/changeEquipmentConnection"	//STB 네트워크 연결상태 변경

	, SERVER_STATUS_SOFTWARE :				"/mgmt/pulling/statusSoftware"
	, SERVER_SOFTWARE_DOWNLOAD :			"/mgmt/download/downloadProgram"
	, SERVER_REPORT_STATUS_SOFTWARE :		"/mgmt/pulling/programDeployStatusResult"

	, SERVER_LIVE_TEXT_DEPLOY_DATA :		"/campaign/livetext/getLiveTextDeployData"
	, SERVER_LIVE_TEXT_UPDATE_DEPLOY_RESULT : "/campaign/livetext/updateLiveTextDeployResult"

	, SERVER_UPLOAD_EQUIPMENT_INFO :		"/mgmt/equipment/uploadEquipmentInfo"		//시리얼번호 업데이트하는 API
		
	, SERVER_GET_DEPLOY_SCHEDULE :			"/campaign/getDeploySchedule"				// 배포 스케줄 조회
	, SERVER_INSERT_STATUS_DEPLOY :			"/campaign/insertStatusDeploy"				// 배포 상태 업데이트
// ===== END NETWORK DEFINE =====
		
		
// ===== START SCHEDULE DEFINE =====	
	, DEPLOY_STATUS_NONE				:	0
	, DEPLOY_STATUS_ShDownSuccess		: 110	// 스케줄 수신 완료.
	, DEPLOY_STATUS_ContentDownSuccess	: 150	// 컨텐츠 다운로드 완료.
	, DEPLOY_STATUS_ShDownFail			: 610	// 스케줄 수신 오류.
	, DEPLOY_STATUS_ContentDownFail		: 650	// 컨텐츠 다운로드 오류.
	, DEPLOY_STATUS_ShParserFail		: 700	// 스케줄 파서 오류.
	, DEPLOY_STATUS_DeploySuccess		: 900	// 배포 완료(스케줄 재생 시작)
// ===== END SCHEDULE DEFINE =====
	
	
// ===== START PATH DEFINE =====
	, DEFAULT_STORAGE_PATH :		"/media/internal/ssp/"
	, DEPLOY_SCHEDULE_FILE : 		"/media/internal/ssp/deploySchedule.txt"
	, DEPLOY_SCHEDULE_FILE_TMP : 	"/media/internal/ssp/deploySchedule_tmp.txt"
	
	, DEFAULT_APP_DOWNLOAD_DIR_PATH : "/media/internal/ssp/deploy/"			 //  "/sdcard" 뒤에 붙을 폴더명
		
	, DEFAULT_MIRRORING_DIR_PATH : "/media/internal/ssp/mirroring/"			 //  "/sdcard" 뒤에 붙을 폴더명
	, DEFAULT_MIRRORING_FILE_NAME : "screenshot_compress.jpg"
		
	, CONTENT_TYPE_IMAGE :		"im" 	//이미지.
	, CONTENT_TYPE_MOVIE :		"mo" 	//동영상.
	, CONTENT_TYPE_PPT :		"pp" 	//파워포인트.
	, CONTENT_TYPE_PACKAGE :	"pa" 	//저작(패키지)
	, CONTENT_TYPE_SOUND :		"so" 	//사운드.
	, CONTENT_TYPE_APP :		"ap" 	//앱 컨텐츠

	//스케쥴의 컨텐츠 저장위치.
	, CONTENT_TYPE_IM_PATH : "/media/internal/ssp/contents/im" //이미지.
	, CONTENT_TYPE_MO_PATH : "/media/internal/ssp/contents/mo" //동영상.
	, CONTENT_TYPE_PP_PATH : "/media/internal/ssp/contents/pp" //파워포인트.
	, CONTENT_TYPE_PA_PATH : "/media/internal/ssp/contents/pa" //저작(패키지)
	, CONTENT_TYPE_SO_PATH : "/media/internal/ssp/contents/so" //사운드.
	, CONTENT_TYPE_AP_PATH : "/media/internal/ssp/contents/ap" //앱컨텐츠.
	, CONTENT_TYPE_DEF_PATH : "/media/internal/ssp/" //default 컨텐츠.
	, CONTENT_TYPE_TEMP_PATH : "/media/internal/ssp/temp" //download temp.
// ===== END PATH DEFINE =====
};

module.exports = Define;

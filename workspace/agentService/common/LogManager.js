var LogManager = {
	error : function(_tag, _logErroCode, _log) {
		var errorLog = {
			equipmentHistorySoftwareCode : 1,
			equipmentHistoryDateTime : this.getLogDate(),
			equipmentHistoryLayerCode : 0,
			equipmentHistoryPackageName : "",
			equipmentHistoryTag : _tag,
			equipmentHistoryContents : _log,
			logErrorCode : _logErroCode
		};
		
		console.error(JSON.stringify(errorLog));

		this.saveErroLog(errorLog);
	},
	saveErroLog : function(_erroLog) {
		var errorLogList = getStorageData("errorLogList");
		if(!errorLogList) {
			errorLogList = [];
		}
		
		errorLogList.push(_erroLog);
		
		setStorageData("errorLogList", errorLogList);
	},
	getLogDate : function() {
		var nowDate = new Date();
		
		var years = nowDate.getFullYear();
		var month = nowDate.getMonth() + 1;
		var date = nowDate.getDate();
		var hours = nowDate.getHours();
		var minutes = nowDate.getMinutes();
		var seconds = nowDate.getSeconds();
		
		month = month >= 10 ? month : '0' + month;
		date = date >= 10 ? date : '0' + date;
		hours = hours >= 10 ? hours : '0' + hours;
		minutes = minutes >= 10 ? minutes : '0' + minutes;
		seconds = seconds >= 10 ? seconds : '0' + seconds;
		
		return years + "" + month + "" + date + "" + hours + "" + minutes + "" + seconds;
	}
};

module.exports = LogManager;

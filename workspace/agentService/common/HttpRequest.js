var Define = require("./Define.js");
//var https = require('https');
var https = require('http');
var fs = require('fs');

var HttpRequest = {
	options : {
		hostname: Define.SVC_DOMAIN,
		port: 9000,//443,
		method: "POST",
		path: "/",
		headers: {
			'Content-Type': 'application/json',
	        'Cache-Control': 'no-cache'
		}
	},
	post : function(_path, _postData, _successCallback, _failCallback) {
		try {
			var postData = "";
			if(_postData) {
				postData = JSON.stringify(_postData);
			}
			
			this.options.path = _path;
			
			var req = https.request(this.options, function(_response) {
				_response.setEncoding("utf8");
				
				var body = "";
				
				_response.on("data", function(_chunk) {
					body += _chunk;
				});
				
				_response.on("end", function() {
					if(body) {
						body = JSON.parse(body);
					}
					
					console.log(body);

					_successCallback(body);
				});
			});
			
			req.setTimeout(1000 * 30);
			
			req.on('error', function(_err) {
				console.log(_err);
				
				_failCallback(_err);
			});
			
			req.on('timeout', function(_err) {
				console.log(_err);
				
				_failCallback(_err);
			});
			
			if(postData) {
				req.write(postData);
			}
			
			req.end();
		} catch(_err) {
			console.log(_err);
			_failCallback(_err);
		}
	},
	sendFile : function(_path, _postData, _fileName, _successCallback, _failCallback) {
		try {
			this.options.path = _path;
			
			var req = https.request(this.options, function(_response) {
				var body = "";
				
				_response.on("data", function(_chunk) {
					body += _chunk;
				});
				
				_response.on("end", function() {
					if(body) {
						body = JSON.parse(body);
					}
					
					console.log(body);

					_successCallback(body);
				});
			});
			
			var data = fs.readFileSync(_fileName);

		    var crlf = "\r\n",
		        boundaryKey = Math.random().toString(16),
		        boundary = '--' + boundaryKey,
		        delimeter = crlf + '--' + boundary,
		        headers = [
                  'Content-Disposition: form-data; name="token"' + crlf,
                  'Content-Disposition: form-data; name="equipmentMirrorStopYn"' + crlf,
		          'Content-Disposition: form-data; name="mirrorFile"; filename="' + _fileName + '"' + crlf
		        ],
		        closeDelimeter = delimeter + '--',
		        multipartBody;

		    multipartBody = Buffer.concat([
		        new Buffer(delimeter + crlf + headers[0] + crlf),
		        new Buffer(_postData.token),
		        new Buffer(delimeter + crlf + headers[1] + crlf),
		        new Buffer(_postData.equipmentMirrorStopYn),
		        new Buffer(delimeter + crlf + headers[2] + crlf),
		        data,
		        new Buffer(closeDelimeter)]
		    );
		    
		    req.setHeader('Content-Type', 'multipart/form-data; boundary=' + boundary);
		    req.setTimeout(1000 * 30);
		    
		    req.write(multipartBody);
		    
			req.on('error', function(_err) {
				console.log(_err);
				_failCallback(_err);
			});
			
			req.on('timeout', function(_err) {
				console.log(_err);
				
				_failCallback(_err);
			});
			
			req.end();
		} catch(_err) {
			console.log(_err);
			_failCallback(_err);
		}
	},
	download : function(_path, _postData, _filePath, _progressCallback, _successCallback, _failCallback) {
		var tempFilePath = Define.CONTENT_TYPE_TEMP_PATH + '/content1.tmp';
		
		try {
			var postData = "";
			if(_postData) {
				postData = JSON.stringify(_postData);
			}
			
			if (!fs.existsSync(Define.CONTENT_TYPE_TEMP_PATH)) {
				fs.mkdirSync(Define.CONTENT_TYPE_TEMP_PATH, 0777);
			}
			
			this.options.path = _path;
			
			var file = fs.createWriteStream(tempFilePath);
			var req = https.request(this.options, function(_response) {
				try {
					var len = parseInt(_response.headers['content-length'], 10);
					var downloaded = 0;
					var preProgress = 0;
					
					_response.on('data', function(chunk) {
						try {
							file.write(chunk);
							
							downloaded += chunk.length;
							
							var progress = Math.round(100 * downloaded / len);
							
							if(preProgress < progress) {
								preProgress = progress;
								_progressCallback(progress);
							}
						} catch(_Ex) {
							console.log(_Ex);
							_failCallback(_Ex);
						}
					}).on('end', function () {
						file.end();
						fs.rename(tempFilePath, _filePath, function (err) {
							if (err){
								fs.unlinkSync(tempFilePath);
								_failCallback(err);
							} else {
								_successCallback();
							}
						});
					}).on('error', function (err) {
						console.log(err);
						
						fs.unlinkSync(tempFilePath);
						_failCallback(err);
		            });
				} catch(_err) {
					console.log(_err);
					
					fs.unlinkSync(tempFilePath);
					_failCallback(_err);
				}
			});
			
			req.setTimeout(1000 * 30);
			
			req.on('error', function(_err) {
				console.log(_err);
				fs.unlinkSync(tempFilePath);
				
				_failCallback(_err);
			});
			
			req.on('timeout', function(_err) {
				console.log(_err);
				fs.unlinkSync(tempFilePath);
				
				_failCallback(_err);
			});
			
			if(postData) {
				req.write(postData);
			}
			
			req.end();
		} catch(_err) {
			console.log(_err);
			
			fs.unlinkSync(tempFilePath);
			_failCallback(_err);
		}
	}
};

module.exports = HttpRequest;
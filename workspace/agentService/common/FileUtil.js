var fs = require("fs");

var FileUtil = {
	writeFile : function(_fullFilePath, _data, _callback) {
		fs.writeFile(_fullFilePath, _data, function (err) {
			if (err) {
				_callback({
					returnValue : false,
					errorText : err.message
				});
			} else {
				_callback({
					returnValue : true
				});
			}
		});
	},
	readFile : function(_fullFilePath) {
		fs.readFile(_fullFilePath, function (err, messageData) {
			if (err) {
				_callback({
					returnValue : false,
					errorText : err.message
				});
			} else {
				_callback({
					returnValue : true,
					data : messageData.toString()
				});
			}
		});
	},
	deleteFile : function(_fullFilePath) {
		fs.unlinkSync(_fullFilePath);
	},
	makeDirectory : function(_filePath) {
		if (!fs.existsSync(_filePath)) {
			fs.mkdirSync(_filePath, 0777);
		}
	},
	isFileExist : function(_filePath) {
		return fs.existsSync(_filePath);
	},
	rename : function(_oldPath, _newPath) {
		fs.renameSync(_oldPath, _newPath);
	},
	readDirectory : function(_filePath) {
		return fs.readdirSync(_filePath);
	},
	getFileSize : function(_filePath) {
		var stats = fs.statSync(_filePath);
		if(stats)
			return stats.size;
		else
			return -1;
	}
};

module.exports = FileUtil;
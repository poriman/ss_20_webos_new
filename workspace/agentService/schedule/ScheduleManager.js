var Define = require("../common/Define.js");
var FileUtil = require('../common/FileUtil.js');
var HttpRequest = require("../common/HttpRequest.js");
var LogManager = require("../common/LogManager.js");

var ScheduleManager = {
	isBusy : false,	
	DOWNLOAD_URL : "/contents/download",
	token : null,
	deployScheduleGroupId : null,
	deplayScheduleTempObject : null,
	downloadCallback : null,
	downloadFinishCallback : null,
	tempScheduleJson : null,
	shManagerList : [],
	shContentDbBeanList : [],
	currentContentsDetailList : null,
	totalDownloadingContentFileSize : 0,
	currentDownloadingContentFileSize : 0,
	doingIndex : 0,
	totalFreeExternal: 0,
	startStatusDeploy : function(_token, _totalFreeExternal, _scheduleStatusData) {
		this.isBusy = true;
		this.token = _token;
		this.totalFreeExternal = _totalFreeExternal;
		this.processScheduleUpdateData(_scheduleStatusData);
	},
	processScheduleUpdateData : function(_scheduleStatusData) {
		try {
			// 스케줄 배포 체크
			if(_scheduleStatusData.scheduleDeployYn == "Y") {
				// 배포 스케줄 있음.
				this.deploySchedule(_scheduleStatusData.scheduleGroupSerialNumber);
				
			} else if(_scheduleStatusData.scheduleDeployYn == "N") {
				// 배포 스케줄 없음. 
				//-> 스케줄 다운로드만 안하고 나머지는 스케줄 배포와 같이 처리하도록 되어있는데.. 필요한가...??

				this.isBusy = false;
			}
			else{
				this.isBusy = false;
			}
			
			// 스케줄 배포 중단 체크
			if(_scheduleStatusData.scheduleDeployStopYn == "Y") {
				// 아무 동작 하지 않음
			}
			
			// 운영자 텍스트(실시간 공지) 배포여부
			if(_scheduleStatusData.liveTextDeployYn == "Y") {
				// 운영자 텍스트(실시간 공지) 배포
			} else if(_scheduleStatusData.liveTextDeployYn == "N") {
				// 운영자 텍스트(실시간 공지) 배포 중단
			}
		} catch(_Ex) {
			console.error(_Ex);
			this.finishDownload("FAIL", _Ex);
		}
	},
	getShManagerListItem : function() {
		try {
			if(this.shManagerList && this.shManagerList.length > 0) {
				return this.shManagerList[this.shManagerList.length - 1];
			}
		} catch(_Ex) {
			console.error(_Ex);
			this.finishDownload("FAIL", _Ex);
		}
		
		return null;
	},
	deploySchedule : function(_scheduleGroupId) {
		try {
			if(this.deployScheduleGroupId && this.deployScheduleGroupId == _scheduleGroupId) {
				var shManagerBean = this.getShManagerListItem();
				if(shManagerBean) {
					
					/// 배포 완료된 스케줄 상태 업데이트 하는 로직 - 배포된 스케줄이 없다는 오류가 나타남 확인 필요
					this.insertStatusDeploy(shManagerBean, Define.DEPLOY_STATUS_ShDownSuccess);
				}
				
			//	return;
			}
			
			this.deployScheduleGroupId = _scheduleGroupId;
			this.getDeploySchedule();	
		} catch(_Ex) {
			console.error(_Ex);
			this.finishDownload("FAIL", _Ex);
		}
	},
	getDeploySchedule : function() {
		try {
			var that = this;
			
			var postData = { token: that.token };
			HttpRequest.post(Define.SERVER_GET_DEPLOY_SCHEDULE, postData, function(_succssData) {
				if(_succssData.retCode == "S0000") {
					that.responseGetDeploySchedule(_succssData);
				}
				else {
					that.finishDownload("FAIL", _succssData);
				}
			}, function(_err) {
				LogManager.error("SchedulerMain", "SCH405", "<< SCH405 >> schedule deploy fail : " + this.deployScheduleGroupId);
				that.insertStatusDeploy(Define.DEPLOY_STATUS_ShDownFail);
				that.finishDownload("FAIL", _err);
			});
		} catch(_Ex) {
			console.error(_Ex);
			this.finishDownload("FAIL", _Ex);
		}
	},
	responseGetDeploySchedule : function(_response) {
		try {
			var id_idx = 0;
			var bParcerable = false;
			var responseJson = JSON.stringify(_response);
			var responseJsonObject = _response;
			var wholeJsonObject = responseJsonObject.deployScheduleInfo;
			if (wholeJsonObject && wholeJsonObject.scheduleList) {
				for (var i = 0; i < wholeJsonObject.scheduleList.length; i++) {
					var playlistObject = wholeJsonObject.scheduleList[i];
					if (playlistObject && playlistObject.playList) {
						for (var j = 0; j < playlistObject.playList.length; j++) {
							var object = playlistObject.playList[j];
							object.playlistId = this.padZero(id_idx, 3);
							id_idx++;
							
							if(object && object.playlistContentsList) {
								for(var k = 0; k < object.playlistContentsList.length; k++) {
									var playlostContentListItem = object.playlistContentsList[k];
									if(playlostContentListItem && playlostContentListItem.splitContentsList) {
										playlostContentListItem.splitContentsList.sort(function(a, b) {
											return parseInt(a.sequence, 10) - parseInt(b.sequence, 10);
										});
									}
								}
							}
						}
						
						bParcerable = true;
					}
				}
			}
			
			var finalResoponseJson = bParcerable ? JSON.stringify(responseJsonObject) : responseJson;

			//	hsshin
			this.deplayScheduleTempObject = finalResoponseJson;
			
			this.processDeployScheduleData(finalResoponseJson);
		} catch(_Ex) {
			LogManager.error("SchedulerMain", "SCH405", "<< SCH405 >> schedule deploy fail : " + this.deployScheduleGroupId);
			console.error(_Ex);
			this.finishDownload("FAIL", _Ex);
		}
	},
	processDeployScheduleData : function(_data) {
		try {
			this.tempScheduleJson = _data;
			
			var tmpData = JSON.parse(_data);
			
			var playedShScheduleServerBean = {
				deploySerialNumber : tmpData.deploySerialNumber,
				deployGroupSerialNumber : tmpData.deployGroupSerialNumber,
				deployDateTime : tmpData.deployDateTime,
				deployScheduleInfo : tmpData.deployScheduleInfo
			}
			
			this.shManagerList.push(playedShScheduleServerBean);
			
			this.insertStatusDeploy(playedShScheduleServerBean, Define.DEPLOY_STATUS_ShDownSuccess);
			this.setScheduleData(playedShScheduleServerBean.deployScheduleInfo);
		} catch(_Ex) {
			LogManager.error("SchedulerMain", "SCH406", "<< SCH406 >> schedule json error, id : " + this.deployScheduleGroupId);
			console.error(_Ex);
			this.finishDownload("FAIL", _Ex);
		}
	},
	setScheduleData : function(_data) {
		try {
			this.makeContentsDirs();
			
//			if(this.shContentDbBeanList.length > 0) {
//				this.shContentDbBeanList.splice(0, this.shContentDbBeanList.length);
//			}
			this.shContentDbBeanList = [];

			// HSSHIN 다운로드 목록을 변수에 기록 (필요없는 컨텐츠 삭제용 리스트)
			this.currentContentsDetailList = _data.contentsDetailList;
			
			if(!_data.contentsDetailList) {
				this.finishDownload("SUCCESS");
				return;
			}
						
			var contentsListObjectList = _data.contentsDetailList;
			if(contentsListObjectList && contentsListObjectList.length > 0) {
				for(var i = 0; i < contentsListObjectList.length; i++) {
					contentsListObject = contentsListObjectList[i];
					if(contentsListObject.playContentsId) {
						var path = this.getContentLocalPath(contentsListObject.type);
						if(path) {
							if(Define.CONTENT_TYPE_PPT.toLowerCase() == contentsListObject.type.toLowerCase()) {
								
							} else if(Define.CONTENT_TYPE_PPT.toLowerCase() == contentsListObject.type.toLowerCase()) {
								path = path + "/" + contentsListObject.playContentsId + "." + "zip";
							} else {
								path = path + "/" + contentsListObject.playContentsId + "." + contentsListObject.contentsExt.toLowerCase();
							}
							
							contentsListObject.contentsPath = path;
							this.addShContentDbBeanList(contentsListObject);
						}
					}
				}
			}
			
			//이미 다운로드가 완료 되어 있으므로 다운로드를 진행 하지 않고 리턴 처리 한다.
			if (this.shContentDbBeanList.length <= 0) {
				var shManagerBean = this.getShManagerListItem(); 
				if(shManagerBean) {
					this.insertStatusDeploy(shManagerBean, Define.DEPLOY_STATUS_ContentDownSuccess);
					this.insertStatusDeploy(shManagerBean, Define.DEPLOY_STATUS_DeploySuccess);
				}
				
//				if (FileUtil.isFileExist(Define.DEPLOY_SCHEDULE_FILE_TMP)) {
//					//임시 스케쥴 파일이 있는 경우
//					if (FileUtil.isFileExist(Define.DEPLOY_SCHEDULE_FILE)) {
//						//이미 deploySchedule 파일이 존재할 경우 기존 파일을 지운후
//						FileUtil.deleteFile(Define.DEPLOY_SCHEDULE_FILE);
//					}
//					//임시로 다운로드 된 스케쥴 파일로 교체한다.
//					FileUtil.rename(Define.DEPLOY_SCHEDULE_FILE_TMP, Define.DEPLOY_SCHEDULE_FILE);
//				}
				
				this.finishDownload("SUCCESS");

				return;
			}
			
			this.totalDownloadingContentFileSize = 0;
			this.currentDownloadingContentFileSize = 0;
			for(var i = 0; i < this.shContentDbBeanList.length; i++) {
				this.totalDownloadingContentFileSize += parseFloat(this.shContentDbBeanList[i].contentsFileSize);
			}
			
			if(this.totalFreeExternal < this.totalDownloadingContentFileSize) {
				this.finishDownload("FAIL", "NOT_ENOUGHT_DISK.");
				return;
			}

			this.setSchedulerContentDownloadBeanList(this.shContentDbBeanList);
		} catch(_Ex) {
			console.error(_Ex);
			this.finishDownload("FAIL", _Ex);
	}		
	},
	setSchedulerContentDownloadBeanList : function(_list) {
		this.doingIndex = 0;
		
		this.startContentDownload(_list);
	},
	startContentDownload : function(_list) {
		try {
			if(_list.length > 0 && _list.length > this.doingIndex) {
				
				this.currentDownloadingContentFileSize += parseFloat(_list[this.doingIndex].contentsFileSize);
				
//				this.downloadCallback({ total: _list.length, current: this.doingIndex + 1, totalSize: this.totalDownloadingContentFileSize, currSize : this.currentDownloadingContentFileSize,  data: _list[this.doingIndex]});
				
				this.startDownload(_list);
			} else {
				this.finishDownload("SUCCESS");
			}
			
		}
		catch (e) {
			this.finishDownload("FAIL", e);
		}
	},
	startDownload : function(_list) {
		try {
			var that = this;
			
			var downloadContent = _list[this.doingIndex];
			
			if(downloadContent && downloadContent.contentsPath) {
				var isDownload = true;
				if(FileUtil.isFileExist(downloadContent.contentsPath)){
					// 파일이 존재하면 다운로드를 진행하지 않고 스킵
					try {
						if((FileUtil.getFileSize(downloadContent.contentsPath) + '') != downloadContent.contentsFileSize) {
							
							FileUtil.deleteFile(downloadContent.contentsPath);
							isDownload = true;
						} else {
							(function(index) {
								that.downloadCallback({
									total: _list.length,
									current: index + 1,
									progress : 100,  
									data: _list[index]
								});
							})(that.doingIndex);
							
							that.doingIndex++;
							
							downloadContent.createTime = new Date().getTime();
							
							that.startContentDownload(that.shContentDbBeanList);	
							
							isDownload = false;
						}
					} catch(_Ex) {
						console.error(_Ex);
						isDownload = false;
						this.finishDownload("FAIL", _Ex);
					}
				}
				
				if(isDownload) {
					// 파일이 존재하지 않는다면...
					var url = this.DOWNLOAD_URL + "/" + downloadContent.playContentsId + "/" + downloadContent.version;
					var postData = { token: that.token };
					
					FileUtil.makeDirectory(Define.DEFAULT_APP_DOWNLOAD_DIR_PATH);
					
					/// 컨텐츠 다운로드 록직 - 파일 다운로드 시 파라메터 오류로 나타남
					HttpRequest.download(url, postData, downloadContent.contentsPath, function(_progressData) {
						that.downloadCallback({
							total: _list.length,
							current: that.doingIndex + 1,
							progress : _progressData,  
							data: _list[that.doingIndex]
						});
					}, function(_successData) {
						try {
							that.doingIndex++;
							
							downloadContent.createTime = new Date().getTime();
							
							that.startContentDownload(that.shContentDbBeanList);	
						} catch(_Ex) {
							console.error(_Ex);
							that.finishDownload("FAIL", _Ex);
						}
					}, function(_err) {
						console.log(_err);
						that.finishDownload("FAIL", _err);
					});
				}
			}
		} catch(_Ex) {
			console.error(_Ex);
			this.finishDownload("FAIL", _Ex);
		}
	},
	finishDownload : function(_status, _err) {
		try {
			var shManagerBean = this.getShManagerListItem();
			
			if(_status == "SUCCESS") {
				this.cleanContents(this.currentContentsDetailList);
				
				if(shManagerBean) {
					this.insertStatusDeploy(shManagerBean, Define.DEPLOY_STATUS_ContentDownSuccess);
					this.insertStatusDeploy(shManagerBean, Define.DEPLOY_STATUS_DeploySuccess);
				}
				
				if (FileUtil.isFileExist(Define.DEPLOY_SCHEDULE_FILE)) {
					//이미 deploySchedule 파일이 존재할 경우 기존 파일을 지운후
					FileUtil.deleteFile(Define.DEPLOY_SCHEDULE_FILE);
				}
				
				this.fileWrite(Define.DEPLOY_SCHEDULE_FILE, this.deplayScheduleTempObject);
			} else if(_status == "FAIL") {
				if(shManagerBean) {
					this.insertStatusDeploy(shManagerBean, Define.DEPLOY_STATUS_ContentDownFail);
				}
				
				this.removeNewShManagerList();
			}

			this.downloadFinishCallback(_status, _err);
			this.isBusy = false;
		} catch(_Ex) {
			console.error(_Ex);
			this.isBusy = false;
		}
	},
	cleanContents : function (_list) {
		try {
			// TO-DO : 삭제 로직 구현
			
			var folderList = ['mo', 'im'];
			
			for(var i = 0 ; i < folderList.length ; ++i) {
				
				var fPath = "/media/internal/ssp/contents/" + folderList[i] + "/";
				
				var files = FileUtil.readDirectory( fPath );
				
				if(files) {
					
					for(var idx = 0 ; idx < files.length ; ++idx) {
						
						var filename = files[idx];
				
						var isRemoveFile = true;

						if(!_list) {
							isRemoveFile = true;
						}
						else {
							try
							{
								for(var s = 0 ; s < _list.length ; ++s) {
									var downloadedfile = _list[s].contentsId + "." + _list[s].contentsExt;
									if(filename == downloadedfile){
										
										isRemoveFile = false;
										break;
									}
									
									if(filename.indexOf(_list[s].contentsId) > -1) {
										isRemoveFile = false;
									}
								}
							}
							catch (_ex) {
								isRemoveFile = false;
							}
						}
						
						if(isRemoveFile) {
							FileUtil.deleteFile(fPath + filename);
						}
					}
				}
			}
			
		} catch(_Ex) {
			console.error(_Ex);
		}
	},
	insertStatusDeploy : function(_shManagerBean, _status) {
		try {
			var deployGroupSerialNumber = _shManagerBean.deployGroupSerialNumber;
			if (!deployGroupSerialNumber
					|| deployGroupSerialNumber.toLowerCase() == "null") {
				deployGroupSerialNumber = 0;
			}
			var postData = {
				token : this.token,
				deploySerialNumber : _shManagerBean.deploySerialNumber,
				deployGroupSerialNumber : deployGroupSerialNumber,
				deployStatement : _status
			};
			HttpRequest.post(Define.SERVER_INSERT_STATUS_DEPLOY, postData,
					function(_successData) {
						console.log("insertStatusDeploy() : "
								+ JSON.stringify(_successData));
					}, function(_err) {
						console.log("insertStatusDeploy() : "
								+ JSON.stringify(_err));
					});
		} catch (e) {
			console.log(e);
		}
	},
	removeNewShManagerList : function() {
		this.shManagerList.splice(0, this.shManagerList.length);
		
		this.deployScheduleGroupId = null;
	},
	addShContentDbBeanList : function(_contentsListObject) {
		try {
			var length = this.shContentDbBeanList.length;
			for(var i = 0; i < length; i++) {
				if(this.shContentDbBeanList[i].playContentsId == _contentsListObject.playContentsId) {
					return;
				}
			}
			
			this.shContentDbBeanList.push(_contentsListObject);
		} catch(_Ex) {
			console.error(_Ex);
		}
	},
	getContentLocalPath : function(_type) {
		if(Define.CONTENT_TYPE_IMAGE.toLowerCase() == _type.toLowerCase()) {
			return Define.CONTENT_TYPE_IM_PATH; 
		} else if(Define.CONTENT_TYPE_MOVIE.toLowerCase() == _type.toLowerCase()) {
			return Define.CONTENT_TYPE_MO_PATH; 
		} else if(Define.CONTENT_TYPE_PPT.toLowerCase() == _type.toLowerCase()) {
			return Define.CONTENT_TYPE_PP_PATH; 
		} else if(Define.CONTENT_TYPE_PACKAGE.toLowerCase() == _type.toLowerCase()) {
			return Define.CONTENT_TYPE_PA_PATH; 
		} else if(Define.CONTENT_TYPE_SOUND.toLowerCase() == _type.toLowerCase()) {
			return Define.CONTENT_TYPE_SO_PATH; 
		} else if(Define.CONTENT_TYPE_APP.toLowerCase() == _type.toLowerCase()) {
			return Define.CONTENT_TYPE_AP_PATH; 
		}
		
		return false;
	},
	makeContentsDirs : function() {
		try {
			//켄텐츠 저장 디렉토리 검사.
			FileUtil.makeDirectory(Define.DEFAULT_STORAGE_PATH);
			FileUtil.makeDirectory(Define.DEFAULT_STORAGE_PATH + "contents/");
			FileUtil.makeDirectory(Define.DEFAULT_APP_DOWNLOAD_DIR_PATH);
			
			FileUtil.makeDirectory(Define.CONTENT_TYPE_IM_PATH);
			FileUtil.makeDirectory(Define.CONTENT_TYPE_MO_PATH);
			FileUtil.makeDirectory(Define.CONTENT_TYPE_PP_PATH);
			FileUtil.makeDirectory(Define.CONTENT_TYPE_PA_PATH);
			FileUtil.makeDirectory(Define.CONTENT_TYPE_SO_PATH);
			FileUtil.makeDirectory(Define.CONTENT_TYPE_AP_PATH);
		} catch(_Ex) {
			console.error(_Ex);
		}
	},
	fileWrite : function(_filePath, _data) {
		FileUtil.writeFile(_filePath, _data, function(_ret) {
			if(_ret.returnValue === false) {
				console.log("fileWrite ERROR : " + _ret.errorText);
			}
		});
	},
	padZero : function(_number, _length) {
		var s = _number + "";
		while(s.length < _length) {
			s = "0" + s;
		}
		
		return s;
	}
};

module.exports = ScheduleManager;
var NetworkDefine = {
	RET_CODE_S0001 : "S0001" //미등록
	, RET_CODE_S0002 : "S0002" //기등록
	, RET_CODE_E0001 : "E0001" //조회 요청 Mac Address 유효하지 않음

	, RET_CODE_S0000 : "S0000" //정상 처리 결과 공통 사용되는 정보
	, RET_CODE_E0000 : "E0000" //등록 실패
	//	, RET_CODE_E0001 : "E0001" //Mac Address 정보가 존재하지 않습니다.
	, RET_CODE_E0002 : "E0002" //개통 승인 번호 정보가 존재하지 않습니다.
	, RET_CODE_E0003 : "E0003" //STB 모델명 정보가 존재하지 않습니다.
	, RET_CODE_E0004 : "E0004" //장치 화면 방향 (orientation) 정보가 존재하지 않습니다.
	, RET_CODE_E0005 : "E0005" //STB Serial 정보가 존재하지 않습니다.
	, RET_CODE_E0006 : "E0006" //장치 지원 해상도 정보가 존재하지 않습니다.
	, RET_CODE_E0007 : "E0007" //펌웨이 버전 정보가 존재하지 않습니다
	, RET_CODE_E0008 : "E0008" //App 목록 정보가 존재하지 않습니다.
	, RET_CODE_E0009 : "E0009" //: Token 정보가 존재하지 않습니다.
	, RET_CODE_E5601 : "E5601" //실시간 공지 대상 데이터 없을시 에러코드


	, RET_CODE_E0014 : "E0014" // : 장치명 정보가 존재하지 않습니다.

	, RET_CODE_E1001 : "E1001" //요청하신 개통승인번호는 유효하지 않습니다.
	, RET_CODE_E1002 : "E1002" //요청하신 펌웨어 버전정보가 유효하지 않습니다.
	, RET_CODE_E1003 : "E1003" //등록되지 않는 STB 장치 Token 발급 요청입니다
	, RET_CODE_E1004 : "E1004" //: 등록되지 않는 STB 장치 Token입니다.
	, RET_CODE_E1005 : "E1005" // : 유효시간 만료된 Token입니다.
	, RET_CODE_E1006 : "E1006" //등록되지 않는 APP 정보입니다
	, RET_CODE_E1012 : "E1012" //인증되지 않은 STB 장치입니다.
};

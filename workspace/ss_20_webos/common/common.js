var CURRENT_PAGE = "index";

function isPlayerScreen() {
	return CURRENT_PAGE == "singleplayer" ? true : false;
}

function getStorageData(_key) {
    if (window.localStorage && window.localStorage.getItem(_key) !== null) {
    	return JSON.parse(window.localStorage.getItem(_key));
    }
    
    return false;
}

function setStorageData(_key, _val) {
    if (!window.localStorage) {
        return false;
    }

    window.localStorage.setItem(_key, JSON.stringify(_val));

    return true;
}

function removeStorageData(_key) {
    if (!window.localStorage) {
        return false;
    }

    window.localStorage.removeItem(_key);

    return true;
}

function isControlCommand(command) {
	if(command && command.substring(0, 1) == 'C') {
		return true;
	}
	
	return false;
}

function showToast(_message, _forceShow, _delay) {
//	if(!_delay) {
//		_delay = 5000;
//	}
//	
//	if(_forceShow || isPlayerScreen() == false) {
//		$.notify({
//			message : _message
//		}, {
//			type : "warning",
//			z_index : 9999,
//			delay : _delay,
//			newest_on_top: true
//		});
//	}
	
	if(_forceShow || isPlayerScreen() == false) {
		var options = {
			msg: _message
		};
		
		var utility = new Utility();
		utility.createToast(function() {}, function(cbObject) {
			console.log("Error Code [" + errorCode + "]: " + errorText);
		}, options);		
	}
}

function getAgentStartDate() {
	var nowDate = new Date();
	
	var years = nowDate.getFullYear();
	var month = nowDate.getMonth() + 1;
	var date = nowDate.getDate();
	var hours = nowDate.getHours();
	var minutes = nowDate.getMinutes();
	var seconds = nowDate.getSeconds();
	
	month = month >= 10 ? month : '0' + month;
	date = date >= 10 ? date : '0' + date;
	hours = hours >= 10 ? hours : '0' + hours;
	minutes = minutes >= 10 ? minutes : '0' + minutes;
	seconds = seconds >= 10 ? seconds : '0' + seconds;
	
	return years + "." + month + "." + date + " " + hours + ":" + minutes + ":" + seconds;
}

function checkValidationEquipmentName(_equipmentName) {
//	var re = /^[ㄱ-ㅎ가-힣0-9a-zA-z()_-]{1,20}$/g;
	var re = /[\{\}\[\]\/?.,;:|*~`!^+<>@\#$%&\\\=\'\"]/g;
    return !re.test(_equipmentName);
}

function replaceSpace(_str) {
	if(_str) {
		return _str.replace(/\s/g, "&nbsp;");	
	}
	
	return _str;
}

function getScreeSize() {
	var retScreen = {
		width : screen.width,
		height: screen.height
	};
	
	try {
		var mode = getStorageData("portraitMode");
		mode = mode ? mode : "off";
		
		if(screen) {
			if(mode == "90") {
				retScreen.width = screen.width > screen.height ? screen.height : screen.width;
				retScreen.height = screen.width > screen.height ? screen.width : screen.height;
			} else {
				retScreen.width = screen.width > screen.height ? screen.width : screen.height;
				retScreen.height = screen.width > screen.height ? screen.height : screen.width;
			}
		}		
	} catch(_Ex) {
		console.error(_Ex);
	}
	
	return retScreen;
}
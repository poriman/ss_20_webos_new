var NETWORK_INFO_INTERVAL_ID = 0;
var NETWORK_INFO_INTERVAL_DELAY = 1000 * 5;

var contentpath = '../../contents/m1.mp4';
var currentcomponent = 'unknown';
var playtime = 5.0;
var currentcontentid = null;

function goto_home() {
	   location.replace("../../index.html");
}

function init()
{  
	initContentIndex();
	
	contentTimer = setTimeout(function() {nextScreen();}, 1000); 
	//contentTimer = setTimeout(function() {runPlaylist();}, 1000); 

    ///	NETWORK 관련 정보 호출
	try {
		getNetworkInfo();
	}
	catch(e) {console.log(e);}
	
	NETWORK_INFO_INTERVAL_ID = setInterval(function() {
		try {
			var oldMAC = MAC_ADDRESS;
			getNetworkInfo(function () {
				if(MAC_ADDRESS != oldMAC)
					existEquipment();
			});
		}
		catch (e) {console.log("Error : " + e);}

	}, NETWORK_INFO_INTERVAL_DELAY);

	startStatusSchedule();
	startStatusEquipment();
	startSendErrorLog();

    /*
    var schedReq2= webOS.service.request("luna://com.lg.app.signage.schedulerservice",
	        {
		        method:"checkfile",
	            parameters:{},
	            onSuccess: function (args) {
	            	if(args && args.returnValue)
	            		console.log("checkfile result:" + args.returnValue + ", " + args.errorText);
	            	else
		            	console.log("error: ", args);
	            	
					currentcomponent = "MO";
					contentpath = "file:///media/internal/10020931.mp4";
					playtime = 30;
					playContents(contentpath, currentcomponent, playtime);
					
					this.cancel();

	            },
	            onFailure: function (args) {
	            	console.log("error: ", args);
	            },
	            subscribe: true
	        });
     */
}

var contentTimer = null;

function playContents(filename, type,  sec)
{
	if(type=="MO") playVideo(filename, sec);
	else if(type=="IM") playImage(filename, sec);
	else if(type=="WE") playiFrame(filename, sec);
	//if(type=="video") playVideo(filename, sec);
	//else if(type=="image") playImage(filename, sec);
	//else if(type=="WE") playiFrame(filename, sec);
	else{
		console.log("unknown contents");
	}
 
	contentTimer = setTimeout(function() {
    	nextScreen();
		}, (sec * 1000) + 50); 
 }


function loadContents(filename, type,  sec)
{
	if(type=="MO") loadVideo(filename, sec);
	else{
	}
	
	backgroundLoaded = true;
 }

function initContentIndex() {
    var schedReq= webOS.service.request("luna://com.lg.app.signage.schedulerservice",
	        {
		        method:"initContentIndex",
	            parameters:{},
	            onSuccess: function (args) {
	            	if(args) console.log("init:", args);
	            },
	            onFailure: function (args) {
	            	console.error("error: ", args);
	            }
	        });
}

var currentVideoId = "myVideo2";
var backgroundLoaded = false;

function nextScreen() {
	try {
		console.log("nextScreen called!");

		var isDone = false;
		
		setTimeout(function() {
			if(!isDone) {
				nextScreen();
			}
		}, 5000);
		
	    var schedReq= webOS.service.request("luna://com.lg.app.signage.schedulerservice",
		        {
			        method:"nextContent",
		            parameters:{},
		            onSuccess: function (args) {
		            	try {
							if(args && args.returnValue == true && args.content){
								console.log("content:", args.returnValue);
								
								if(args.content.contentsId != currentcontentid) {
									backgroundLoaded = false;
								}
								
								if(!backgroundLoaded) {
									currentcontentid = args.content.contentsId;
									currentcomponent = args.content.type;
									contentpath = "file:///media/internal/ssp/contents/" + args.content.type.toLowerCase() + "/" + args.content.contentsId + "." + args.content.contentsExt;
									playtime = args.content.playTime;
									
									loadContents(contentpath, currentcomponent, playtime);
									playContents(contentpath, currentcomponent, playtime);

									currentcontentid = args.nextContent.contentsId;
									currentcomponent = args.nextContent.type;
									contentpath = "file:///media/internal/ssp/contents/" + args.nextContent.type.toLowerCase() + "/" + args.nextContent.contentsId + "." + args.nextContent.contentsExt;
									playtime = args.nextContent.playTime;
									
									loadContents(contentpath, currentcomponent, playtime);
								}
								else {
									playContents(contentpath, currentcomponent, playtime);

									currentcontentid = args.nextContent.contentsId;
									currentcomponent = args.nextContent.type;
									contentpath = "file:///media/internal/ssp/contents/" + args.nextContent.type.toLowerCase() + "/" + args.nextContent.contentsId + "." + args.nextContent.contentsExt;
									playtime = args.nextContent.playTime;

									loadContents(contentpath, currentcomponent, playtime);
								}
															
								document.getElementById('hdPlayYN').value = "Y";
								document.getElementById('hdCurrentScheduleId').value = args.schedule.scheduleId;
								document.getElementById('hdCurrentContentId').value = currentcontentid;

								document.getElementById('notiText').innerHTML = "컨텐트: " + args.content.contentsId + "." + args.content.contentsExt + " 타입: " + args.content.type + " 재생시간: " + args.content.playTime;
							}
							else {
								clear();
								
								setTimeout(function() {
									DisplayControl(CurrentDisplayStatus = "Screen Off", function (err) {
										if(!err) {
											goto_home();
										}
									});
								}, 1000); 
							}
		            		isDone = true;
		            	} catch(_Ex) {
		            		console.error(_Ex);
		            	}
		            },
		            onFailure: function (args) {
		            	console.error(args);
						clear();
						setTimeout(function() {
							DisplayControl(CurrentDisplayStatus = "Screen Off", function (err) {
								if(!err) {
									goto_home();
								}
							});
						}, 1000);
		            },
		            onComplete: function (args) {
		            	console.log("next call Complete");
		            }
		        });
	}
	catch (e)
	{
    	console.error(e);
		clear();
		setTimeout(function() {
			DisplayControl(CurrentDisplayStatus = "Screen Off", function (err) {
				if(!err) {
					goto_home();
				}
			});
		}, 1000); 
	
	}
}

function runPlaylist() {
	try {
		
	    var schedReq= webOS.service.request("luna://com.lg.app.signage.schedulerservice",
		        {
			        method:"getPlaylist",
		            parameters:{},
		            onSuccess: function (args) {
						if(args && args.returnValue == true && args.playlist){
							console.log("content:", args.returnValue);
							
							var gapless = new GaplessPlayback('gaplessPlayer', 'gaplessPlaylist');
							//gapless.setContentPath('ipk');
							
							for(var idx = 0 ; idx < args.playlist.length ; ++idx) {
								var content = args.playlist[idx];
								currentcomponent = content.type;
								contentpath = "file:///media/internal/ssp/contents/" + content.type.toLowerCase() + "/" + content.contentsId + "." + content.contentsExt;
								playtime = content.playTime;
								
								gapless.addContent(contentpath, playtime);
								
							}

							gapless.setObjectFit('fill');
							gapless.play();
							
							document.getElementById('gaplessPlayer').style.visibility = 'visible';
							
							document.getElementById('hdPlayYN').value = "Y";
							document.getElementById('hdCurrentScheduleId').value = args.schedule.scheduleId;
							//document.getElementById('hdCurrentContentId').value = args.content.contentsId;


							//document.getElementById('notiText').innerHTML = "컨텐트: " + args.content.contentsId + "." + args.content.contentsExt + " 타입: " + currentcomponent + " 재생시간: " + playtime;
						}
						else {
							clear();
							
							setTimeout(function() {
								DisplayControl(CurrentDisplayStatus = "Screen Off", function (err) {
									if(!err) {
										goto_home();
									}
								});
							}, 1000); 
						}
		            },
		            onFailure: function (args) {
		            	console.log("error: ", args);
						clear();
						setTimeout(function() {
							DisplayControl(CurrentDisplayStatus = "Screen Off", function (err) {
								if(!err) {
									goto_home();
								}
							});
						}, 1000); 
		            }
		        });
	}
	catch (e)
	{
    	console.log("catch error: ", args);
		clear();
		setTimeout(function() {
			DisplayControl(CurrentDisplayStatus = "Screen Off", function (err) {
				if(!err) {
					goto_home();
				}
			});
		}, 1000); 
	
	}
}

function loadVideo(filename, sec) {
	    
	var videoId = currentVideoId == "myVideo" ? "myVideo2" : "myVideo";
	
	var video=document.getElementById(videoId);
    
    video.setAttribute('src', filename);
    video.load();
     
	console.log("loadVideo(" + filename + ")");

}

function playVideo(filename, sec) {
	
	var videoId = currentVideoId == "myVideo" ? "myVideo2" : "myVideo";
	var backVideoId = currentVideoId != "myVideo" ? "myVideo2" : "myVideo";

	var video=document.getElementById(videoId);
    
    video.play();
    
    currentVideoId = currentVideoId == "myVideo" ? "myVideo2" : "myVideo";
    
   	setTimeout(function () {
   		document.getElementById(videoId).style.visibility = 'visible';
   		document.getElementById(backVideoId).style.visibility = 'hidden';
   		document.getElementById('myImage').style.visibility = 'hidden';
   	}, 100);
 
	console.log("playVideo(" + filename + ")");

}

function playImage(filename, sec) {
	
    var video=document.getElementById("myVideo");
    video.pause();
    
    var image=document.getElementById("myImage");
    image.setAttribute('src', filename);

	console.log("playImage(" + filename + ")");

	setTimeout(function() {
		document.getElementById("myVideo").style.visibility = 'hidden';
		document.getElementById('myImage').style.visibility = 'visible';		
	}, 50);
}

function playiFrame(filename, sec) {
    var video=document.getElementById("myVideo");
    video.pause();

    console.log("playiframe(" + filename + ")");
	document.getElementById('myVideo').style.visibility = 'hidden';
	document.getElementById('myImage').style.visibility = 'hidden';
}

function clear(){
    var video=document.getElementById("myVideo");
    video.pause();

	document.getElementById('myVideo').style.visibility = 'hidden';
	document.getElementById('myImage').style.visibility = 'hidden';	
	
	document.getElementById('notiText').innerHTML = "다음스케줄이 없습니다.";
	
	document.getElementById('hdPlayYN').value = "";
	document.getElementById('hdCurrentScheduleId').value = "";
	document.getElementById('hdCurrentContentId').value = "";
	
}





	 

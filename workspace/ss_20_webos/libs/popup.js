var Popup = {
	popupCount : 1,
	popupId: "ssp_webos_popup",
	popupBodyId: this.popupId + "_body",
	popupElement: null,
	closeCallback : null,
	status : "close",
	init : function() {
		if(this.popupElement == null) {
			this.createElement();
		} else {
			this.closePopup();
		}
	},
	createElement : function() {
		var that = this;
		
		$_dialog = $("<div id='" + this.popupId + "' class='modal fade' role='dialog' data-backdrop='static' data-keyboard='false' style='z-index: 9999;'></div>");
		$_modal = $("<div class='modal-dialog modal-sm'></div>");
		$_modalContent = $("<div class='modal-content'></div>");
		$_modalBody = $("<div id='" + this.popupBodyId + "' class='modal-body'></div>");
		$_footer = $("<div id='modal-footer' class='modal-footer text-center'><button type='button' class='btn btn-popup-confirm-ok' data-dismiss='modal'></button></div>");
		
		$_modalContent.append($_modalBody);
		$_modalContent.append($_footer);
		$_modal.append($_modalContent);
		$_dialog.append($_modal);
		
		this.popupElement = $_dialog;
		
		$('body').prepend($_dialog);
		
		$("#modal-footer > button").off("click").on("click", function() {
			console.log("footer click");
			that.closePopup();
		});
	},
	showPopup : function(_message, _autoClose, _callback) {
		console.log("popupCount : " + (this.popupCount++));
		var that = this;
		
		this.init();
		
		$("#" + this.popupBodyId).html(_message);
		
		this.popupElement.modal('show');
		
		this.status = "open"; 
		
		if(_callback) {
			this.closeCallback = _callback;
		}
		
		if(_autoClose) {
			setTimeout(function() {
				if(that.status == "open") {
					that.closePopup();
				}
			}, 3000);
		}
	},
	closePopup : function() {
		this.popupElement.modal('hide');
		
		this.status = "close";
		
		if(this.closeCallback) {
			this.closeCallback();
		}
	},
	showToast : function(_message) {
		
	}
};

module.exports = Popup;
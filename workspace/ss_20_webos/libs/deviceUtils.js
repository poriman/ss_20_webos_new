function getNetworkInfo(_callback) {
	try {
		var deviceInfo = new DeviceInfo();
		deviceInfo.getNetworkInfo(function(_successData) {
			if (_successData.wired.state == "connected") {
				NETWORK_INFO = _successData.wired;
				NETWORK_INFO.connectType = 2;
			} else if (_successData.wifi.state == "connected") {
				NETWORK_INFO = _successData.wifi;
				NETWORK_INFO.connectType = 1;
			} else {
				showToast("네트워크 접속이 확인 되지 않습니다.\n연결상태를 체크하시기 바랍니다.");
				
				if(NETWORK_INFO) {
					LogManager.error("DeviceControlManager", "MID301", "<< MID301 >> Network Cable disconnected !");
				}
				
				MAC_ADDRESS = null;
				NETWORK_INFO = null;
				
				if(_callback) {
					_callback(MAC_ADDRESS);
				}
				return;
			}
			
			if(NETWORK_INFO && NETWORK_INFO.isInternetConnectionAvailable === false) {
				LogManager.error("DeviceControlManager", "MID302", "<< MID302 >> Network state = NETSTATE_DISCONNECTED");
			}

			var deviceInfo = new DeviceInfo();
			deviceInfo.getNetworkMacInfo(function(_successData) {
				if(NETWORK_INFO) {
					if (NETWORK_INFO.connectType == 2 && _successData.wiredInfo && _successData.wiredInfo.macAddress) {
						MAC_ADDRESS = _successData.wiredInfo.macAddress;
					} else if (NETWORK_INFO.connectType == 1 && _successData.wifiInfo && _successData.wifiInfo.macAddress) {
						MAC_ADDRESS = _successData.wifiInfo.macAddress;
					} else {
						if(NETWORK_INFO.connectType == 2) {
							LogManager.error("DeviceControlManager", "EQA402", "<< EQA402 >> tth0 mac address is null");
						} else if(NETWORK_INFO.connectType == 1) {
							LogManager.error("DeviceControlManager", "EQA402", "<< EQA402 >> wifi mac address is null");
						}
						//showToast("MAC Address 조회 실패.");
					}
				}
				
	
				$("#DEVICE_MAC_ADDRESS").text(MAC_ADDRESS);
	
				if (_callback) {
					_callback(MAC_ADDRESS);
				}
			}, function(_failData) {
				LogManager.error("DeviceControlManager", "EQA402", "<< EQA402 >> mac address is null");
				
				console.error("getNetworkInfo() MAC Error :" + JSON.stringify(_failData));
				if (_callback) {
					_callback(MAC_ADDRESS);
				}
			});
		}, function(_failData) {
			console.log("getNetworkInfo() IP Error :" + JSON.stringify(_failData));

			if (_callback) {
				_callback(MAC_ADDRESS);
			}
		});
	} catch (e) {
		LogManager.error("DeviceControlManager", "EQA402", "<< EQA402 >> tth0 mac address is null");
		console.log("getNetworkInfo :" + e);
		
		if(_callback) {
			_callback(MAC_ADDRESS);
		}
	}
}

function getDeviceMacAddress(_callback) {
	// TODO : emulator에서 테스트용
	MAC_ADDRESS = "00:00:00:00:00:00";
	
	var deviceInfo = new DeviceInfo();
	deviceInfo.getNetworkMacInfo(function(_successData) {
		console.log("getDeviceMacAddress() Success : " + JSON.stringify(_successData));
		
		if(NETWORK_INFO && NETWORK_INFO.connectType == 2 &&
			_successData.wiredInfo && _successData.wiredInfo.macAddress) {
			MAC_ADDRESS = _successData.wiredInfo.macAddress;
		} else if(NETWORK_INFO && NETWORK_INFO.connectType == 1 
			&& _successData.wifiInfo && _successData.wifiInfo.macAddress) {
			MAC_ADDRESS = _successData.wifiInfo.macAddress;
		} else {
			showToast("MAC Address 조회 실패.");
		}
		
		$("#DEVICE_MAC_ADDRESS").text(MAC_ADDRESS);
		
		if(_callback) {
			_callback(MAC_ADDRESS);
		}
	}, function(_failData) {
		console.log("getDeviceMacAddress() Error :" + JSON.stringify(_failData));

		if(_callback) {
			_callback(MAC_ADDRESS);
		}
	});
}

function getDeviceIP() {
	var deviceInfo = new DeviceInfo();
    deviceInfo.getNetworkInfo(function(_successData) {
    	console.log("getDeviceIP() Success :" + JSON.stringify(_successData));
    	
    	if(_successData.wired.state == "connected") {
        	NETWORK_INFO = _successData.wired;
        	NETWORK_INFO.connectType = 2;
		} else if(_successData.wifi.state == "connected") {
			NETWORK_INFO = _successData.wifi;
			NETWORK_INFO.connectType = 1;
		} else {
			showToast("네트워크 접속이 확인 되지 않습니다.</br><span class='main-color'>연결상태를 체크</span>하시기 바랍니다.");
    		return;
		}
    }, function(_failData) {
    	console.log("getDeviceIP() Error :" + JSON.stringify(_failData));
    	
    	// TODO : emulator에서 테스트용
//		Popup.showPopup(_failData.errorCode + " : " + _failData.errorText);
    	NETWORK_INFO = {
    		"interfaceName": "eth0",
	        "state": "connected",
	        "netmask": "255.255.255.0",
	        "ipAddress": "192.168.0.10",
	        "method": "manual",
	        "onInternet": "no",
	        "gateway": "192.168.0.1",
	        "connectType" : "2"
    	};
    });
}

function getDeviceInfo(_callback) {
	var deviceInfo = new DeviceInfo();
    deviceInfo.getPlatformInfo(function(_successData) {
    	console.log("getDeviceInfo() Success :" + JSON.stringify(_successData));
    	getSoundStatus(function(_soundData) {
    		_successData.sound = _soundData;
    		
    		getBeaconInfo(function(_beaconData) {
    			_successData.beacon = _beaconData;
        		_callback(_successData);
    		});
    	});
    }, function(_failData) {
    	console.error("getDeviceInfo() Error :" + JSON.stringify(_failData));
    	getSoundStatus(function(_soundData) {
    		_successData.sound = _soundData;
    		
    		getBeaconInfo(function(_beaconData) {
    			_successData.beacon = _beaconData;
        		_callback(_successData);
    		});
    	});
    });
}

function getSoundStatus(_callback) {
    var sound = new Sound();
    sound.getSoundStatus(function(_successData) {
    	console.log("getSoundStatus() Success :" + JSON.stringify(_successData));
    	_callback(_successData);
    }, function(_failData) {
    	LogManager.error("DeviceControlManager", "MID202", "<< MID202 >> invalid volume get");
    	
    	console.error("getSoundStatus() Error :" + JSON.stringify(_failData));
    	_callback({
    		level : 0,
    		muted : false,
    		externalSpeaker : false
    	});
    });
}

function getBeaconInfo(_callback) {
    var deviceInfo = new DeviceInfo();
    deviceInfo.getBeaconInfo(function(_successData) {
    	console.log("getBeaconInfo() Success :" + JSON.stringify(_successData));
    	_callback(_successData);
    }, function(_failData) {
    	console.error("getBeaconInfo() Error :" + JSON.stringify(_failData));
    	_callback({
    		enabled : false,
    		uuid : "",
    		major : 0,
    		minor: 0
    	});
    });
}

function getPlatformInfo(_callback) {
    var deviceInfo = new DeviceInfo();
    deviceInfo.getPlatformInfo(function(_successData) {
    	console.log("getPlatformInfo() Success :" + JSON.stringify(_successData));
    	
    	if(_callback) {
    		_callback(_successData);
    	}
    }, function(_failData) {
    	console.error("getPlatformInfo() Fail :" + JSON.stringify(_failData));
    	
    	if(_callback) {
    		_callback();
    	}
    });
}
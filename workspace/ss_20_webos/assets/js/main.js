var settingDeviceStep = 0;
var SCHEDULE_STATUS_INTERVAL_ID = 0;
var SCHEDULE_STATUS_INTERVAL_DELAY = 1000 * 10;
var EQUIPMENT_STATUS_INTERVAL_ID = 0;
var EQUIPMENT_STATUS_INTERVAL_DELAY = 1000 * 5;
var EQUIPMENT_APPROVAL_INTERVAL_ID = 0;
var EQUIPMENT_STATUS_INTERVAL_DELAY = 1000 * 5;
var NETWORK_INFO = null;
var MAC_ADDRESS = null;
var shopeEquipment = {};
var MIRRORING_INTERVAL_ID = 0;
var DEFAULT_MIRRORING_LIMIT_TIME = 10000; //10초간
var DEFAULT_MIRRORING_ONE_CYCLE_INTERVAL = 1000; //1초에 한번씩
var MIRRORING_LIMIT = 10;
var mirroringCount = 0;
var SEND_ERROR_LOG_INTERVAL_ID = 0;
var SEND_ERROR_LOG_INTERVAL_DELAY = 1000 * 60;
var CAN_CHANGE_PLAYER = false;
var IS_SHOWING_POPUP = false;

var NOT_REGISTER_EQUIPMENT_TIMEOUT_ID = 0;
var NOT_REGISTER_EQUIPMENT_TIMEOUT_DELAY = 1000 * 30;

var CurrentDisplayStatus = "Active";

function gotoPlayer() {
	location.href = "../components/views/singleplayer.html";
}

function startPlayer(){
	if(isPlayerScreen() == false && CAN_CHANGE_PLAYER == true && IS_SHOWING_POPUP == false) {
		// TO-DO : Check 스케줄
		try {
			hasScheduleContent(function(_successRes) {
				if(_successRes && _successRes.returnValue == true && _successRes.hasContent == true) {
					console.log("has Content:", _successRes.hasContent);
					
					DisplayControl(CurrentDisplayStatus = "Active", function (err) {
						if(!err) {
							gotoPlayer();
						}
					});
				} else {
					DisplayControl(CurrentDisplayStatus = "Screen Off");
				}
			}, function(_failRes) {
				DisplayControl(CurrentDisplayStatus = "Screen Off");
			});
		} catch(_Ex) {
			console.error(_Ex);
			
			DisplayControl(CurrentDisplayStatus = "Screen Off");
		}
	}
}

function hasScheduleContent(_successCallback, _failCallback) {
	try {
		var schedReq= webOS.service.request("luna://com.lg.app.signage.schedulerservice", {
			method: "hasPlayContent",
			parameters: {},
			onSuccess: function (_successRes) {
				if(_successCallback) {
					_successCallback(_successRes);
				}
			},
			onFailure: function (_failRes) {
				if(_failCallback) {
					_failCallback(_failRes)
				}
			}, onComplete : function() {
				console.log("hasPlayContent() complete");
			}
		});
	} catch(_Ex) {
		console.error(_Ex);
		
		_failCallback();
	}
}

function checkMacAddress(_callback) {
	try {
		getNetworkInfo(function(_macAddress) {
			try {
				MAC_ADDRESS = _macAddress;
				_callback(MAC_ADDRESS);
			} catch(_Ex) {
				console.error(_Ex);
			}
		});
	} catch(_Ex) {
		console.error(_Ex);

		// TO-DO : 30초 후에 플레이어 실행 
		CAN_CHANGE_PLAYER = true;
	}
}

function existEquipment() {
	try {
		var isDone = false;
		
		setTimeout(function() {
			if(!isDone) {
				existEquipment();
			}
		}, 10000);
		
		checkMacAddress(function(_macAddress) {
			try {
				if(MAC_ADDRESS == null){
		    		CAN_CHANGE_PLAYER = true;
					return;
				}
				
				var _authKey = getStorageData("authKey");
				if(!_authKey) _authKey = _macAddress;
				
				var params = { macAddress : _authKey };
				console.log("existEquipment() Request - " + JSON.stringify(params));
				
				var request = webOS.service.request("luna://com.lg.app.signage.agentservice/", {
			        method:"existEquipment",
			        parameters: params,
			        onFailure: function(inError) {
			        	console.log("existEquipment() onFailure - " + JSON.stringify(inError));
			        	
			        	// TO-DO : 30초 후에 플레이어 실행 
			    		CAN_CHANGE_PLAYER = true;
			    		
			    		MAC_ADDRESS = null;

			        	showToast(inError.errorCode + " : " + inError.errorText);
			    		return;
			        },
			        onSuccess: function(inResponse) {
			        	try {
			        		console.log("existEquipment() onSuccess - " + JSON.stringify(inResponse));
				        	
				        	if(inResponse.returnValue === true) {
				        		if(inResponse.retCode == NetworkDefine.RET_CODE_S0001) {	// 미등록
				        			removeStorageData("tokenInfo");
				        			removeStorageData("equipmentInfo");
				        			removeStorageData("equipmentRegisterInfo");

				        			removeScheduleFile();

				        			var _authYn = getStorageData("authYn");
				        			
				        			if(_authYn == "Y") {
					        			setStorageData("authYn", "N");
					        			
					        			if(CURRENT_PAGE == "main") {		
					        				location.replace("registerAgain.html");
					        			} else {
					        				location.replace("../../views/registerAgain.html");
					        			}
				        			}
				        			else {
					        			setStorageData("authYn", "N");
					    				showRegisterDevciePopup("register");
				        			}
				        		} else if(inResponse.retCode == NetworkDefine.RET_CODE_S0002) {	// 기등록
				        			setStorageData("authKey", _authKey);
				        			setStorageData("authYn", "Y");
				        			startCheckEquipmentApproval();
				    			} else {
				    				showToast(inResponse.retCode + " : " + inResponse.retMessage);
				    				
				    	        	// TO-DO : 30초 후에 플레이어 실행 
				    				CAN_CHANGE_PLAYER = true;
				    			}
				        	} else {
				        		showToast(inResponse.errorCode + " : " + inResponse.errorText);
				        		
					        	// TO-DO : 30초 후에 플레이어 실행 
				        		CAN_CHANGE_PLAYER = true;
				        		
				        		MAC_ADDRESS = null;
				        	}
			        	} catch(_Ex) {
			        		console.error(_Ex);
			        	}
			        }, onComplete : function() {
						console.log("existEquipment() complete");
						isDone = true;
					},
			        subscribe : false,
			        resubscribe : false
			    });
			} catch(_Ex) {
				console.error(_Ex);
				CAN_CHANGE_PLAYER = true;
			}
		});
	} catch(_Ex) {
		console.error(_Ex);
	}
}

function startCheckEquipmentApproval() {
	stopCheckEquipmentApproval();
	
	checkEquipmentApproval();
	
	EQUIPMENT_APPROVAL_INTERVAL_ID = setInterval(function() {
		checkEquipmentApproval();
	}, EQUIPMENT_STATUS_INTERVAL_DELAY);
}

function stopCheckEquipmentApproval() {
	if(EQUIPMENT_APPROVAL_INTERVAL_ID > 0) {
		clearInterval(EQUIPMENT_APPROVAL_INTERVAL_ID);
		EQUIPMENT_APPROVAL_INTERVAL_ID = 0;
	}
}

function checkEquipmentApproval() {
	checkMacAddress(function(_macAddress) {
		try {
			if(MAC_ADDRESS == null){
				// TO-DO : 30초 후에 플레이어 실행 
	    		CAN_CHANGE_PLAYER = true;
				return;
			}
			
			var tokenInfo = getStorageData("tokenInfo");
			console.log("tokenInfo : " + JSON.stringify(tokenInfo));
			if(!tokenInfo) {
				stopCheckEquipmentApproval();
				createToken();
				return;
			}
			
			var params = { token : tokenInfo.token };
			console.log("checkEquipmentApproval() Request - " + JSON.stringify(params));
			
			var request = webOS.service.request("luna://com.lg.app.signage.agentservice/", {
		        method:"approvalEquipment",
		        parameters: params,
		        onFailure: function(inError) {
		        	console.log("checkEquipmentApproval() onFailure - " + JSON.stringify(inError));
		        	
					// TO-DO : 30초 후에 플레이어 실행 
		    		CAN_CHANGE_PLAYER = true;
		    		return;
		        },
		        onSuccess: function(inResponse) { 
		        	try {
		        		console.log("checkEquipmentApproval() onSuccess - " + JSON.stringify(inResponse));
			        	
			        	if(inResponse.returnValue === true) {
			        		if(inResponse.retCode == NetworkDefine.RET_CODE_S0000) {
			        			if(inResponse.data.equipmentApprovalYn == "Y") {
			        				stopCheckEquipmentApproval();
			        				
			        				$("#waitingText").hide();
			        				$("#btnStartPlayer").show();
			        				
			        				getEquipmentInfo();
			        				
			        	        	// TO-DO : 30초 후에 플레이어 실행 
			        				CAN_CHANGE_PLAYER = true;
			        			} else {
			        				removeScheduleFile();
			        				
			        				$("#waitingText").show();
			        				$("#btnStartPlayer").hide();
			        			}
			        		} else if(inResponse.retCode == NetworkDefine.RET_CODE_E0009 || inResponse.retCode == NetworkDefine.RET_CODE_E1004 || inResponse.retCode == NetworkDefine.RET_CODE_E1005) {
			        			stopCheckEquipmentApproval();
			        			
			        			createToken();
			        			
			    	        	// TO-DO : 30초 후에 플레이어 실행 
			        			CAN_CHANGE_PLAYER = true;
			        		} else if(inResponse.retCode == NetworkDefine.RET_CODE_E1003) {
			        			invalidEquipment()
			        		} else {
			    				stopCheckEquipmentApproval();
			    				
			    				showToast(inResponse.retCode + " : " + inResponse.retMessage);
			    				
			    	        	// TO-DO : 30초 후에 플레이어 실행 
			    				CAN_CHANGE_PLAYER = true;
			    			}
			        	} else {
			        		stopCheckEquipmentApproval();
			        		
			        		showToast(inResponse.errorCode + " : " + inResponse.errorText);
			        		
				        	// TO-DO : 30초 후에 플레이어 실행 
			        		CAN_CHANGE_PLAYER = true;
			        	}
		        	} catch(_Ex) {
		        		console.error(_Ex);
		        	}
		        }, onComplete : function() {
					console.log("checkEquipmentApproval() complete");
				},
		        subscribe : false,
		        resubscribe : false
		    });
		} catch(_Ex) {
			console.error(_Ex);
		}
	});
}

function getEquipmentInfo() {
	checkMacAddress(function(_macAddress) {
		try {
			if(MAC_ADDRESS == null){
				return;
			}
			
			var tokenInfo = getStorageData("tokenInfo");
			if(!tokenInfo) {
				return;
			}
			
			var params = { token : tokenInfo.token };
			console.log("getEquipmentInfo() Request - " + JSON.stringify(params));
			
			var request = webOS.service.request("luna://com.lg.app.signage.agentservice/", {
		        method:"searchEquipmentInfo",
		        parameters: params,
		        onFailure: function(inError) {
		        	console.log("getEquipmentInfo() onFailure - " + JSON.stringify(inError));
		        	
		        	showToast(inError.errorCode + " : " + inError.errorText);
		    		return;
		        },
		        onSuccess: function(inResponse) {  
		        	try {
		        		console.log("getEquipmentInfo() onSuccess - " + JSON.stringify(inResponse));
		            	
		            	if(inResponse.returnValue === true) {
		            		if(inResponse.retCode == NetworkDefine.RET_CODE_S0000) {
		            			var equipmentInfo = inResponse.data;
		            			setStorageData("equipmentInfo", equipmentInfo);
		            			
		            			$("#saveCustomerName").text(equipmentInfo.customerName);
		            			$("#saveShopName").text(equipmentInfo.shopName);
		            			$("#saveEquipmentType").text("싱글장치");
		            			$("#saveEquipmentName").html(replaceSpace(equipmentInfo.equipmentName));
		            			
		            			startStatusSchedule();
		            			
		            			startStatusEquipment();
		            			
		            			startSendErrorLog();
		            		} else if(inResponse.retCode == NetworkDefine.RET_CODE_E0009 || inResponse.retCode == NetworkDefine.RET_CODE_E1004 || inResponse.retCode == NetworkDefine.RET_CODE_E1005) {
		            			createToken();
//		            		} else if(inResponse.retCode == NetworkDefine.RET_CODE_E0014) {
//		            			createToken();
		            		} else {
		            			showToast(inResponse.retCode + " : " + inResponse.retMessage);
		        			}
		            	} else {
		            		showToast(inResponse.errorCode + " : " + inResponse.errorText);
		            	}
		        	} catch(_Ex) {
		        		console.error(_Ex);
		        	}
		        }, onComplete : function() {
					console.log("getEquipmentInfo() complete");
				},
		        subscribe : false,
		        resubscribe : false
		    });
		} catch(_Ex) {
			console.error(_Ex);
		}
	});
}

function registerEquipment(_deviceInfo) {
	checkMacAddress(function(_macAddress) {
		try {
			if(MAC_ADDRESS == null){
				return;
			}
			
			var params = getRegisterParams(_deviceInfo);
			if(params === false) {
				return false;
			}
			
			$(".loading").show();
			
			console.log("registerEquipment() Request - " + JSON.stringify(params));
			
			var request = webOS.service.request("luna://com.lg.app.signage.agentservice/", {
		        method:"registerEquipment",
		        parameters: params,
		        onFailure: function(inError) {
		        	$(".loading").hide();
		        	
		        	console.log("registerEquipment() onFailure - " + JSON.stringify(inError));
		        	
		        	showToast(inError.errorCode + " : " + inError.errorText);
		    		return;
		        },
		        onSuccess: function(inResponse) {
		        	try {
		        		$(".loading").hide(); 
			        	console.log("registerEquipment() onSuccess - " + JSON.stringify(inResponse));
			        	
			        	if(inResponse.returnValue === true) {
			        		if(inResponse.retCode == NetworkDefine.RET_CODE_S0000) {
			        			
			        			setStorageData("authKey", params.equipmentMacAddress);
			        			setStorageData("authYn", "Y");

			        			params.customerName = $.trim($("#preCustomerName").text());
			        			params.shopName = $.trim($("#preShopName").text());
			        			
			        			$("#saveCustomerName").text(params.customerName);
			        			$("#saveShopName").text(params.shopName);
			        			$("#saveEquipmentType").text("싱글장치");
			        			$("#saveEquipmentName").html(replaceSpace(params.equipmentName));
			        			
			        			hideRegisterDevicePopup();
			        			
			        			setStorageData("equipmentRegisterInfo", params);
			        			
			        			$("#waitingText").show();
			        			
			        			createToken();
			        		} else {
			        			showToast(inResponse.retCode + " : " + inResponse.retMessage);
			        		}
			        	} else {
			        		showToast(inResponse.errorCode + " : " + inResponse.errorText);
			        	}
		        	} catch(_Ex) {
		        		console.error(_Ex);
		        	}
		        }, onComplete : function() {
					console.log("registerEquipment() complete");
				},
		        subscribe : false,
		        resubscribe : false
		    });
		} catch(_Ex) {
			console.error(_Ex);
		}
	});
}

function changeEquipmentInfo(_deviceInfo) {
	checkMacAddress(function(_macAddress) {
		try {
			if(MAC_ADDRESS == null){
				return;
			}
			
			var tokenInfo = getStorageData("tokenInfo");
			console.log("changeEquipmentInfo() tokenInfo : " + JSON.stringify(tokenInfo));
			if(!tokenInfo) {
				createToken();
				return;
			}
			
			var params = getUpdateParams(_deviceInfo);
			if(params === false) {
				return false;
			}
			
			params.token = tokenInfo.token;
			
			$(".loading").show();
			
			console.log("changeEquipmentInfo() Request - " + JSON.stringify(params));
			
			var request = webOS.service.request("luna://com.lg.app.signage.agentservice/", {
		        method:"changeEquipmentInfo",
		        parameters: params,
		        onFailure: function(inError) {
		        	$(".loading").hide();
		        	
		        	console.log("changeEquipmentInfo() onFailure - " + JSON.stringify(inError));
		        	
		        	showToast(inError.errorCode + " : " + inError.errorText);
		    		return;
		        },
		        onSuccess: function(inResponse) {
		        	try {
		        		$(".loading").hide(); 
			        	console.log("changeEquipmentInfo() onSuccess - " + JSON.stringify(inResponse));
			        	
			        	if(inResponse.returnValue === true) {
			        		if(inResponse.retCode == NetworkDefine.RET_CODE_S0000) {
			        			hideRegisterDevicePopup();
			        			
			        			$("#saveEquipmentName").html(replaceSpace(params.equipmentName));
			        			
			        			var equipmentInfo = inResponse.data;
			        			
			        			var equipmentInfo = getStorageData("equipmentInfo");
			        			var equipmentStatus = getStorageData("equipmentStatus");
			        			var equipmentRegisterInfo = getStorageData("equipmentRegisterInfo");
			        			var portraitMode = getStorageData("portraitMode");
			        			
			        			if(equipmentInfo) {
			        				equipmentInfo.equipmentName = params.equipmentName;
			        				equipmentInfo.equipmentDirection = params.equipmentScreenDirection;
			        				
			        				setStorageData("equipmentInfo", equipmentInfo);
			        			}
			        			
			        			if(equipmentStatus) {
			        				equipmentStatus.equipmentName = params.equipmentName;
			        				equipmentStatus.equipmentScreenDirection = params.equipmentScreenDirection;
			        				
			        				setStorageData("equipmentStatus", equipmentStatus);
			        			}
			        			
			        			if(equipmentRegisterInfo) {
			        				equipmentRegisterInfo.equipmentName = params.equipmentName;
			        				equipmentRegisterInfo.equipmentScreenDirection = params.equipmentScreenDirection;
			        				
			        				setStorageData("equipmentRegisterInfo", equipmentRegisterInfo);
			        			}
			        			
			        			if(portraitMode) {
			        				portraitMode = params.equipmentScreenDirection == "S00" ? "off" : "90"; 
			        				setStorageData("portraitMode", portraitMode);
			        			}
			        		} else {
			        			showToast(inResponse.retCode + " : " + inResponse.retMessage);
			        		}
			        	} else {
			        		showToast(inResponse.errorCode + " : " + inResponse.errorText);
			        	}
		        	} catch(_Ex) {
		        		console.error(_Ex);
		        	}
		        }, onComplete : function() {
					console.log("changeEquipmentInfo() complete");
				},
		        subscribe : false,
		        resubscribe : false
		    });
		} catch(_Ex) {
			console.error(_Ex);
		}
	});
}

function createToken() {
	checkMacAddress(function(_macAddress) {
		try {
			if(MAC_ADDRESS == null){
				return;
			}
			
			var _authKey = getStorageData("authKey");
			if(!_authKey) _authKey = _macAddress;
			
			var params = {macAddress : _authKey};
			
			console.log("createToken() Request - " + JSON.stringify(params));
			
			var request = webOS.service.request("luna://com.lg.app.signage.agentservice/", {
		        method:"createToken",
		        parameters: params,
		        onFailure: function(inError) {
		        	LogManager.error("ServerDataManager", "TKN302", "<< TKN302 >> create token fail code null");
		        	
		        	console.log("createToken() onFailure - " + JSON.stringify(inError));
		        	
		        	showToast(inError.errorCode + " : " + inError.errorText);
		    		return;
		        },
		        onSuccess: function(inResponse) {  
		        	try {
		        		console.log("createToken() onSuccess - " + JSON.stringify(inResponse));
			        	
			        	if(inResponse.returnValue === true) {
			        		if(inResponse.retCode == NetworkDefine.RET_CODE_S0000) {
			        			var tokenInfo = {
			        				token : inResponse.data.token,
			        				expireDateTime: inResponse.data.expireDateTime
			        			}
			        			
			        			setStorageData("tokenInfo", tokenInfo);
			        			
			        			startCheckEquipmentApproval();
			        		} else if(inResponse.retCode == NetworkDefine.RET_CODE_E1003) {
		            			invalidEquipment();
		            		} else {
			    				LogManager.error("ServerDataManager", "TKN302", "<< TKN302 >> create token fail code null");
			    				showToast(inResponse.retCode + " : " + inResponse.retMessage);
			    			}
			        	} else {
			        		LogManager.error("ServerDataManager", "TKN302", "<< TKN302 >> create token fail code null");
		    				showToast(inResponse.errorCode + " : " + inResponse.errorText);
		    			}
		        	} catch(_Ex) {
		        		console.error(_Ex);
		        	}
		        }, onComplete : function() {
					console.log("createToken() complete");
				},
		        subscribe : false,
		        resubscribe : false
		    });
		} catch(_Ex) {
			console.error(_Ex);
		}
	});
}

function getRegisterParams(_deviceInfo) {
	var params = {};
	
	var _authKey = getStorageData("authKey");
	var screenSize = getScreeSize();
	
	try {
		params = {
			openApprovalNo : $.trim($("#equipmentAuthCode").val()),
	    	equipmentModelName : "WebOSClient",//_deviceInfo.modelName ? _deviceInfo.modelName : "",
	    	equipmentScreenDirection : $(".btn-direction-landscape").hasClass("selected") ? "S00" : "S01",
	    	equipmentMacAddress : _authKey ? _authKey : $.trim($("#DEVICE_MAC_ADDRESS").text()),
	    	equipmentSerialNumber : _deviceInfo.serialNumber ? _deviceInfo.serialNumber : "",
	    	equipmentSupportResolution : screenSize ? screenSize.width + "x" + screenSize.height : "1920x1080",
	    	firmwareVersion : null, //_deviceInfo.firmwareVersion ? _deviceInfo.firmwareVersion : "",
	    	equipmentName : $.trim($("#equipmentName").val()),
	    	equipmentVolumeStatus : "S" + (_deviceInfo.sound ? convertSoundLevel(_deviceInfo.sound.level) : 0),
	    	screenEquipmentVolumeStatus : "S" + (_deviceInfo.sound ? convertSoundLevel(_deviceInfo.sound.level) : 0),
	    	equipmentBeaconStatus : (_deviceInfo.beacon && _deviceInfo.beacon.enabled ? 1 : 0),
	    	equipmentConnectionType : NETWORK_INFO ? NETWORK_INFO.connectType : 0,
	    	appList : [{"appSoftware1" : "com.lg.app.signage"}]
		};
		
		console.log(JSON.stringify(params));
		
		if(params.openApprovalNo.length <= 0) {
			showToast("인증코드를 입력하세요.");

			if(settingDeviceStep != 2) {

 				setTimeout(function() {
					settingDeviceStep = 2;
					updateSettingDeviceStep();
				}, 2000); 
			}

			return false;
		}
		
		if(params.equipmentName.length <= 0) {
			showToast("장치명을 입력해주세요.");
			
			if(settingDeviceStep != 3) {

 				setTimeout(function() {
					settingDeviceStep = 3;
					updateSettingDeviceStep();
				}, 2000); 
			}

			return false;
		} else if(params.equipmentName.length > 20) {
			showToast("장치명을 최대 20자로 입력해주세요.");
			
			if(settingDeviceStep != 3) {

 				setTimeout(function() {
					settingDeviceStep = 3;
					updateSettingDeviceStep();
				}, 2000); 
			}

			return false;
		} else if(!checkValidationEquipmentName(params.equipmentName)) {
			showToast("장치명은 한글, 영문, 숫자, 특수문자('-', '_', '(', ')')만 입력 가능합니다.");
			
			if(settingDeviceStep != 3) {

 				setTimeout(function() {
					settingDeviceStep = 3;
					updateSettingDeviceStep();
				}, 2000); 
			}

			return false;
		}
		
		return params;
	} catch(_Ex) {
		console.error(_Ex);
	}
	
	return false;
}

function getUpdateParams(_deviceInfo) {
	var params = {};
	
	try {
		params = {
	    	equipmentScreenDirection : $(".btn-direction-landscape").hasClass("selected") ? "S00" : "S01",
	    	equipmentName : $.trim($("#equipmentName").val())
		};
		
		console.log(JSON.stringify(params));
		
		if(params.equipmentName.length <= 0) {
			showToast("장치명을 입력해주세요.");
			
			if(settingDeviceStep != 3) {

 				setTimeout(function() {
					settingDeviceStep = 3;
					updateSettingDeviceStep();
				}, 2000); 
			}

			return false;
		} else if(params.equipmentName.length > 20) {
			showToast("장치명을 최대 20자로 입력해주세요.");
			
			if(settingDeviceStep != 3) {

 				setTimeout(function() {
					settingDeviceStep = 3;
					updateSettingDeviceStep();
				}, 2000); 
			}

			return false;
		} else if(!checkValidationEquipmentName(params.equipmentName)) {
			showToast("장치명은 한글, 영문, 숫자, 특수문자('-', '_', '(', ')')만 입력 가능합니다.");
			
			if(settingDeviceStep != 3) {

 				setTimeout(function() {
					settingDeviceStep = 3;
					updateSettingDeviceStep();
				}, 2000); 
			}

			return false;
		}
		
		return params;
	} catch(_Ex) {
		console.error(_Ex);
	}
	
	return false;
}

function convertSoundLevel(_level) {
	return padZero(Math.floor((15 / 100) * _level), 2);
}

function padZero(_number, _length) {
	var s = _number + "";
	while(s.length < _length) {
		s = "0" + s;
	}
	
	return s;
}

function updateSettingDeviceStep() {
	try {
		$("ul.settingTabs > li").removeClass("press");
		$("ul.settingTabs > li:eq(" + settingDeviceStep + ")").addClass("press");
		
		$(".tab-content > .tab-pane").removeClass("active");
		$(".tab-content > .tab-pane:eq(" + settingDeviceStep + ")").addClass("active");
		
		$(".btn-back").show();
		$(".btn-next").show();
		
		if(settingDeviceStep == 0) {
			$(".btn-back").hide();
		} else if(settingDeviceStep == 4) {
			$(".btn-next").hide();
		}
	} catch(_Ex) {
		console.error(_Ex);
	}
}

function showRegisterDevciePopup(_mode) {
	try {
		IS_SHOWING_POPUP = true;
		
		$("#modalPopup").modal('show');
		
		var osVersion = getStorageData('webOSVersion');
		if(osVersion) {
			if(parseInt(osVersion.substr(0, 1), 10) >= 3) {
				var portraitMode = getStorageData("portraitMode");
				if(portraitMode && portraitMode == "90") {
					$(".btn-direction-portrait").addClass("selected");
					$(".btn-direction-landscape").removeClass("selected");
				} else {
					$(".btn-direction-portrait").removeClass("selected");
					$(".btn-direction-landscape").addClass("selected");
				}
			} else {
				$(".btn-direction-portrait").removeClass("selected");
				$(".btn-direction-landscape").addClass("selected");
				$(".btn-direction-portrait").hide();
			}
		} else {
			$(".btn-direction-portrait").removeClass("selected");
			$(".btn-direction-landscape").addClass("selected");
			$(".btn-direction-portrait").hide();
		}

		if(_mode == "edit") {
			$(".settingTabs").addClass("edit");
			$(".settingTabs > li:eq(1)").addClass("hidden");
			$(".settingTabs > li:eq(2)").addClass("hidden");
		} else if(_mode == "register") {
			$(".settingTabs").removeClass("edit");
			$(".settingTabs > li:eq(1)").removeClass("hidden");
			$(".settingTabs > li:eq(2)").removeClass("hidden");
			
			getNetworkInfo();
			
			$("#DEVICE_IP").text("000.000.000.000");
			if(NETWORK_INFO) {
				$("#DEVICE_IP").text(NETWORK_INFO.ipAddress);
			}
			
			$(".btn-popup-confirm-ok").off("click").on("click", function(event) {
				try {
					event.stopPropagation();
					
					$("#equipmentAuthCodeYN").val("N");
					
					var equipmentAuthCode = $.trim($("#equipmentAuthCode").val());
					if(equipmentAuthCode.length <= 0) {
						showToast("인증코드를 입력하세요.");
						$("#equipmentAuthResult").text("인증코드를 입력하세요.");
						$("#equipmentAuthIcon").attr("src", "../assets/images/icon_code_fail.png");
						$("#equipmentAuthResult").parent("div").show();
						
						if(settingDeviceStep != 2) {

     	    				setTimeout(function() {
    							settingDeviceStep = 2;
    							updateSettingDeviceStep();
	    					}, 2000); 
						}
						
						return;
					}
					
					getShopEquipment(equipmentAuthCode);
				} catch(_Ex) {
					console.error(_Ex);
				}
			});
			
			$("#equipmentAuthCode").off("focus").on("focus", function() {
				$("#equipmentAuthResult").text("");
				$("#equipmentAuthResult").parent("div").hide();
				$("#equipmentAuthCodeYN").val("N");
			});
		}
		
		updateSettingDeviceStep();
		
		$(".btn-direction-landscape").off("click").on("click", function(event) {
			event.stopPropagation();
			
			if(!$(this).hasClass("selected")) {
				$(this).addClass("selected");
				$(".btn-direction-portrait").removeClass("selected");
				
				setPortraitMode('off');
			}
		});
		
		$(".btn-direction-portrait").off("click").on("click", function(event) {
			event.stopPropagation();
			
			if(!$(this).hasClass("selected")) {
				$(this).addClass("selected");
				$(".btn-direction-landscape").removeClass("selected");
				
				setPortraitMode('90');
			}
		});
		
		$(".btn-complete").off("click").on("click", function(event) {
			event.stopPropagation();
			
			getDeviceInfo(function(_deviceInfo) {
				if(_mode == "register") {
					registerEquipment(_deviceInfo); 
				} else if(_mode == "edit") {
					changeEquipmentInfo(_deviceInfo); 
				}
			});
		});
		
		$(".btn-back").off("click").on("click", function(event) {
			try {
				event.stopPropagation();
				
				settingDeviceStep--;
				
				if(_mode == "edit" && (settingDeviceStep == 1 || settingDeviceStep == 2)) {
					settingDeviceStep = 0;
				}
				
				if(settingDeviceStep < 0) {
					settingDeviceStep = 0;
				}
				
				updateSettingDeviceStep();
			} catch(_Ex) {
				console.error(_Ex);
			}
		});
			
		$(".btn-next").off("click").on("click", function(event) {
			try {
				event.stopPropagation();
				
				if(_mode == "edit" && settingDeviceStep == 0) {
					var equipmentInfo = getStorageData("equipmentInfo");
					if(equipmentInfo) {
						$("#equipmentName").val(equipmentInfo.equipmentName);
					}
				}
				
				if(settingDeviceStep == 2) {
					if(_mode == "register") {
						if($.trim($("#equipmentAuthCode").val()).length <= 0) {
							showToast("인증코드를 입력하세요.");
							$("#equipmentAuthIcon").attr("src", "../assets/images/icon_code_fail.png");
							return false;
						}
						
						if($.trim($("#equipmentAuthCodeYN").val()) != "Y") {
							showToast("인증코드를 입력 후 확인을 눌러주세요.");
							$("#equipmentAuthIcon").attr("src", "../assets/images/icon_code_fail.png");
							return false;	
						}
					}
					
					
				} else if(settingDeviceStep == 3) {
					var tmpEquipmentName = $.trim($("#equipmentName").val());
					if(tmpEquipmentName.length <= 0) {
						showToast("장치명을 입력해주세요.");
						return false;
					} else if(tmpEquipmentName.length > 20) {
						showToast("장치명을 최대 20자로 입력해주세요.");
						return false;
					} else if(!checkValidationEquipmentName(tmpEquipmentName)) {
						showToast("장치명은 한글, 영문, 숫자, 특수문자('-', '_', '(', ')')만 입력 가능합니다.");
						return false;
					}

					$("#preCustomerName").text(shopeEquipment.customerName);
					$("#preShopName").text(shopeEquipment.shopName);
					$("#preEquipmentType").text($(".btn-single").hasClass("selected") ? "싱글장치" : "멀티장치");
					$("#preEquipmentName").html(replaceSpace(tmpEquipmentName));
					$("#preDisplayOrientation").text($(".btn-direction-landscape").hasClass("selected") ? "가로" : "세로");
				}
					
				settingDeviceStep++;
				
				if(_mode == "edit" && (settingDeviceStep == 1 || settingDeviceStep == 2)) {
					settingDeviceStep = 3;
				}
				
				if(settingDeviceStep > 4) {
					settingDeviceStep = 4;
				}
				
				updateSettingDeviceStep();
			} catch(_Ex) {
				console.error(_Ex);
			}
		});
	} catch(_Ex) {
		console.error(_Ex);
	}
}

function getShopEquipment(_equipmentAuthCode) {
	checkMacAddress(function(_macAddress) {
		if(MAC_ADDRESS == null){
			return;
		}
		
		var params = { macAddress : _macAddress, equipmentAuthCode : _equipmentAuthCode };
		
		console.log("getShopEquipment() Request - " + JSON.stringify(params));
		
		var request = webOS.service.request("luna://com.lg.app.signage.agentservice/", {
	        method:"shopEquipment",
	        parameters: params,
	        onFailure: function(inError) {
	        	console.log("getShopEquipment() onFailure - " + JSON.stringify(inError));
	        	
	        	showToast(inError.errorCode + " : " + inError.errorText);
	    		return;
	        },
	        onSuccess: function(inResponse) {
	        	try {
	        		console.log("getShopEquipment() onSuccess - " + JSON.stringify(inResponse));
		        	
		        	if(inResponse.returnValue === true) {
		        		if(inResponse.retCode == NetworkDefine.RET_CODE_S0000) {
			        		shopeEquipment.customerName = inResponse.data.customerName;
			        		shopeEquipment.shopName = inResponse.data.shopName;
			        		shopeEquipment.equipmentName = inResponse.data.equipmentName;
		        			$("#equipmentName").val(shopeEquipment.equipmentName);
		        			$("#equipmentAuthResult").text("인증 되었습니다.");
		        			$("#equipmentAuthCodeYN").val("Y");
		        			$("#equipmentAuthIcon").attr("src", "../assets/images/icon_code_pass.png");
		    			} else if(inResponse.retCode == NetworkDefine.RET_CODE_E1001) {
		    				$("#equipmentAuthIcon").attr("src", "../assets/images/icon_code_fail.png");
		    				$("#equipmentAuthResult").text("인증코드가 유효하지 않습니다.");
		    			} else {
		    				$("#equipmentAuthIcon").attr("src", "../assets/images/icon_code_fail.png");
		    				$("#equipmentAuthResult").text(inResponse.retMessage);
		    			}
		        	} else {
		        		$("#equipmentAuthIcon").attr("src", "../assets/images/icon_code_fail.png");
		        		$("#equipmentAuthResult").text(inResponse.retMessage);
		        	}
		        	
		        	$("#equipmentAuthResult").parent("div").show();
	        	} catch(_Ex) {
	        		console.error(_Ex);
	        	}
	        }, onComplete : function() {
				console.log("getShopEquipment() complete");
			},
	        subscribe : false,
	        resubscribe : false
	    });
	});
}

function hideRegisterDevicePopup() {
	$("#modalPopup").modal('hide');
	resetPopup();
	IS_SHOWING_POPUP = false;
}

function resetPopup() {
	settingDeviceStep = 0;
	
	$(".btn-direction-portrait").removeClass("selected");
	$(".btn-direction-landscape").removeClass("selected");
	$(".btn-direction-landscape").addClass("selected");
	
	$("#equipmentAuthCode").val("");
	$("#equipmentName").val("");
	
	$("#equipmentAuthResult").parent("div").hide();
	shopeEquipment = {};
}

function startStatusSchedule() {
	stopStatusSchedule();
	
	SCHEDULE_STATUS_INTERVAL_ID = setInterval(function() {
		statusSchedule();
	}, SCHEDULE_STATUS_INTERVAL_DELAY);
}

function stopStatusSchedule() {
	if(SCHEDULE_STATUS_INTERVAL_ID > 0) {
		clearInterval(SCHEDULE_STATUS_INTERVAL_ID);
		SCHEDULE_STATUS_INTERVAL_ID = 0;
	}
}

function statusSchedule() {
	try {
//		if(MAC_ADDRESS == null){
//			return;
//		}
		
		var tokenInfo = getStorageData("tokenInfo");
		if(!tokenInfo) {
			stopStatusSchedule();
			
			createToken();
			return;
		}
		
		getStorageInfo(function(_totalExternal, _totalFreeExternal) {
			var freeDisk = (_totalExternal * 1024 * 1024) - (_totalFreeExternal * 1024 * 1024);
			var params = { token : tokenInfo.token, totalFreeExternal: freeDisk };
			
			console.log("statusSchedule() Request - " + JSON.stringify(params));
			
			var request = webOS.service.request("luna://com.lg.app.signage.agentservice/", {
		        method:"statusSchedule",
		        parameters: params,
		        onFailure: function(inError) {
		        	console.log("statusSchedule() onFailure - " + JSON.stringify(inError));
		        	
		        	console.error(inError.errorCode + " : " + inError.errorText);
		        	
		    		return;
		        },
		        onSuccess: function(inResponse) {  
		        	console.log("statusSchedule() onSuccess - " + JSON.stringify(inResponse));
		        	
		        	if(inResponse.returnValue === true) {
		        		if(inResponse.retCode == "DOWNLOAD") {
		        			isDownloading = true;
		        			
		        			if(inResponse.data && inResponse.data.data) {
		        				setDownloadStatus("컨텐트 다운로드 중 (" + inResponse.data.current + "/" + inResponse.data.total + ") : " + inResponse.data.data.contentsId + "." + inResponse.data.data.contentsExt + " (" + inResponse.data.progress + "%)");	
		        			}
		        		} else if(inResponse.retCode == "DOWNLOAD_FINISH") {
		     				try {
		     					console.log("response DOWNLOAD_FINISH : " +  inResponse.data);
		     					
		     					CAN_CHANGE_PLAYER = true;
		     					
		     					if(inResponse.error) {
		     						console.error(inResponse.error);
		     					}
		     					
		     					if(inResponse.data == "FAIL") {
		     						setDownloadStatus("다운로드 중 오류 발생.");
		     					} else {
		     						if($("#downloadStatus")) {
			     	    				setTimeout(function() {
		                    				setDownloadStatus("");
		    	    					}, 3000); 
			        				}		     						
		     					}
		    				}
		    				catch (e) {}
		    			} else if (inResponse.retCode == "SCHEDULE_BUSY") {
		        			console.log("스케줄 처리 중입니다.");
		        		} else {
		            		if(inResponse.retCode == NetworkDefine.RET_CODE_S0000) {

		            		} else if(inResponse.retCode == NetworkDefine.RET_CODE_E0009 || inResponse.retCode == NetworkDefine.RET_CODE_E1004 || inResponse.retCode == NetworkDefine.RET_CODE_E1005) {
		            			createToken();
		            		} else if(inResponse.retCode == NetworkDefine.RET_CODE_E1003) {
		            			invalidEquipment();
		        			} else {
		        				console.error(inResponse.retCode + " : " + inResponse.retMessage);
		        			}
		        		}
		        	} else {
		        		console.error(inResponse.errorCode + " : " + inResponse.errorText);
		        	}
		        }, onComplete : function() {
					console.log("statusSchedule() complete");
				},
		        subscribe : true,
		        resubscribe : false
		    });
		});
	} catch(_Ex) {
		console.error(_Ex);
	}
}

function setDownloadStatus(textStatus) {
	try {
		$("#downloadStatus").text(textStatus);
	}
	catch (e) {
	}
	
//	try {
//		document.getElementById('notiText').innerHTML = textStatus;
//	}
//	catch (e) {
//		
//	}
}

function invalidEquipment() {
	removeStorageData("tokenInfo");
	removeStorageData("equipmentInfo");
	removeStorageData("equipmentRegisterInfo");
	
	removeScheduleFile();
	
	stopStatusSchedule();
	
	stopStatusEquipment();
	
	stopCheckEquipmentApproval();
	
	stopSendErrorLog();
	
//	existEquipment();

	var _authYn = getStorageData("authYn");

	setStorageData("authYn", "N");
	
	/// 서버로 부터 해지 명령이 조회된 경우에만
	if(_authYn == "Y") {
		if(CURRENT_PAGE == "main") {		
			location.replace("registerAgain.html");
		} else {
			location.replace("../../views/registerAgain.html");
		}
	}
	else{
		if(CURRENT_PAGE == "main") {		
			location.replace("main.html");
		} else {
			location.replace("../../views/main.html");
		}
	}
}

function removeScheduleFile() {
	if(MAC_ADDRESS == null){
		return;
	}
	
	var request = webOS.service.request("luna://com.lg.app.signage.agentservice/", {
        method:"removceScheduleFile",
        parameters: {},
        onFailure: function(inError) {
        	console.log("removeScheduleFile() onFailure - " + JSON.stringify(inError));
        },
        onSuccess: function(inResponse) {  
        	console.log("removeScheduleFile() onSuccess - " + JSON.stringify(inResponse));
        },
        subscribe : false,
        resubscribe : false
    });
}

function startStatusEquipment() {
	stopStatusEquipment();
	
	EQUIPMENT_STATUS_INTERVAL_ID = setInterval(function() {
		statusEquipment();
	}, EQUIPMENT_STATUS_INTERVAL_DELAY);
}

function stopStatusEquipment() {
	if(EQUIPMENT_STATUS_INTERVAL_ID > 0) {
		clearInterval(EQUIPMENT_STATUS_INTERVAL_ID);
		EQUIPMENT_STATUS_INTERVAL_ID = 0;
	}
}

function statusEquipment() {
	checkMacAddress(function(_macAddress) {
		if(MAC_ADDRESS == null){
			return;
		}
		
		var tokenInfo = getStorageData("tokenInfo");
		if(!tokenInfo) {
			stopStatusEquipment();
			return;
		}
		
		var connectionType = NETWORK_INFO.connectType == 1 ? "002" : "001";
		
		var appVersion = getStorageData("version");
		if(!appVersion) {
			appVersion = "0.0.0";
		}
		
		var params = { token : tokenInfo.token, equipmentConnectionType: connectionType, equipmentProcessAppList : "com.lg.app.signage(" + appVersion + ")" };
		
		try {
			params.playScheduleSerialNo = document.getElementById('hdCurrentScheduleId').value;
		} catch (e) {}

		try {
			params.playContentsYn = document.getElementById('hdPlayYN').value;
		} catch (e) {}

		console.log("statusEquipment() Request - " + JSON.stringify(params));
		
		var request = webOS.service.request("luna://com.lg.app.signage.agentservice/", {
	        method:"statusEquipment",
	        parameters: params,
	        onFailure: function(inError) {
	        	console.log("statusEquipment() onFailure - " + JSON.stringify(inError));
	        	
	        	return;
	        },
	        onSuccess: function(inResponse) {  
	        	console.log("statusEquipment() onSuccess - " + JSON.stringify(inResponse));
	        	
	        	if(inResponse.returnValue === true) {
	        		if(inResponse.retCode == NetworkDefine.RET_CODE_S0000) {
	        			var equipmentStatus = inResponse.data;
	        			
	        			setStorageData("equipmentStatus", equipmentStatus);
	        			processStatusReport(equipmentStatus);
	        		} else if(inResponse.retCode == NetworkDefine.RET_CODE_E0009 || inResponse.retCode == NetworkDefine.RET_CODE_E1004 || inResponse.retCode == NetworkDefine.RET_CODE_E1005) {
	        			stopStatusEquipment();
	        			
	        			createToken();
	        		} else if(inResponse.retCode == NetworkDefine.RET_CODE_E1003) {
	        			stopStatusEquipment();
	        			invalidEquipment();
	    			} else {
    					console.error(inResponse.retCode + " : " + inResponse.retMessage);
	    			}
	        	} else {
					console.error(inResponse.errorCode + " : " + inResponse.errorText);
	        	}
	        }, onComplete : function() {
				console.log("statusEquipment() complete");
			},
	        subscribe : false,
	        resubscribe : false
	    });
	});
}

function reportStatusEquipment(_isReboot, _reportStatusEquipmentOA) {
	try {
		if(MAC_ADDRESS == null){
			return;
		}
		
		var tokenInfo = getStorageData("tokenInfo");
		if(!tokenInfo) {
			return;
		}
		
		var params = {};
		
		params.token = tokenInfo.token;

		params.equipmentScreenStatus = _reportStatusEquipmentOA.equipmentScreenStatus; //장치 화면 상태
		params.equipmentScreenDirection = _reportStatusEquipmentOA.equipmentScreenDirection; //장치 화면 방향
		params.equipmentRestartStatus = _reportStatusEquipmentOA.equipmentRestartStatus; //장치 재시작  여부 결과
		params.equipmentVolumeStatus = _reportStatusEquipmentOA.equipmentVolumeStatus; //장치 볼륨 상태
		params.screenEquipmentVolumeStatus = _reportStatusEquipmentOA.screenEquipmentVolumeStatus; //모니터 볼륨 상태 [1~15]
		params.equipmentWifiStatus = _reportStatusEquipmentOA.equipmentWifiStatus; //장치 WIFI 동작 상태
		params.equipmentWifiSsid = _reportStatusEquipmentOA.equipmentWifiSsid; //장치 WIFI SSID 
		params.equipmentWifiSsidPassword = _reportStatusEquipmentOA.equipmentWifiSsidPassword;;//장치 WIFI SSID 패스워드
		params.equipmentWifiConnectionUrl = _reportStatusEquipmentOA.equipmentWifiConnectionUrl; //장치 WIFI 접속 URL
		params.equipmentBeaconStatus = _reportStatusEquipmentOA.equipmentBeaconStatus; //장치 BEACON 
		params.equipmentHardwareCollectStatus = _reportStatusEquipmentOA.equipmentHardwareCollectStatus; //장치 하드웨어 수집 
		params.equipmentCpuUseVolume = _reportStatusEquipmentOA.equipmentCpuUseVolume; //장치 CPU 사용량 [단위 :%]
		params.equipmentCpuTemperature = _reportStatusEquipmentOA.equipmentCpuTemperature; //장치 CPU 온도
		params.equipmentMemoryUseVolume = _reportStatusEquipmentOA.equipmentMemoryUseVolume; //장치 메모리 사용량 [단위 :%]
		params.equipmentSdCardTotalVolume = _reportStatusEquipmentOA.equipmentSdCardTotalVolume; //장치 SD 카드 전체 용량 [단위 :Mbtye]
		params.equipmentSdCardUseVolume = _reportStatusEquipmentOA.equipmentSdCardUseVolume; //장치 SD 카드 사용량 [단위 :Mbtye]
		params.equipmentScreenOutputStatus = _reportStatusEquipmentOA.equipmentScreenOutputStatus; //장치 화면 출력 상태 [S00 : 장치 화면 출력 완료 성공, C00 : 장치 화면 출력 명령, F00 : 장치 화면 출력 실패]
		params.equipmentWifiType = _reportStatusEquipmentOA.equipmentWifiType; //장치 Wifi 유형 [S00:AP, S01:Sensing, C00: AP 제어명령, C01:Sensing 제어명령, F00:AP 제어실패, F01:Sensing 제어실패]
		
		console.log("reportStatusEquipment() Request - " + JSON.stringify(params));
		
		var request = webOS.service.request("luna://com.lg.app.signage.agentservice/", {
	        method:"equipmentStatusResult",
	        parameters: params,
	        onFailure: function(inError) {
	        	console.log("reportStatusEquipment() onFailure - " + JSON.stringify(inError));
	        	return;
	        },
	        onSuccess: function(inResponse) {  
	        	console.log("reportStatusEquipment() onSuccess - " + JSON.stringify(inResponse));
	        	
	        	if(inResponse.returnValue === true) {
	        		if(inResponse.retCode == NetworkDefine.RET_CODE_S0000) {
						if(_isReboot) {
							PowerControl("reboot");
						}
	        		} else if(inResponse.retCode == NetworkDefine.RET_CODE_E0009 || inResponse.retCode == NetworkDefine.RET_CODE_E1004 || inResponse.retCode == NetworkDefine.RET_CODE_E1005) {
	        			createToken();
	        		} else if(inResponse.retCode == NetworkDefine.RET_CODE_E1003) {
	        			invalidEquipment();
	    			} else {
	    				console.log(inResponse.retCode + " : " + inResponse.retMessage);
	    			}
	        	} else {
	        		console.log(inResponse.errorCode + " : " + inResponse.errorText);
	        	}
	        }, onComplete : function() {
				console.log("reportStatusEquipment() complete");
			},
	        subscribe : false,
	        resubscribe : false
	    });
	} catch(_Ex) {
		LogManager.error("MessagePull", "EQS201", "<< EQS201 >> Exception : trace =" + _Ex.message);
		
		console.error(_Ex);
	}
}

function processStatusReport(order) {
	try {
		if(MAC_ADDRESS == null){
			// TO-DO : 30초 후에 플레이어 실행 
			CAN_CHANGE_PLAYER = true;
			//setTimeout(function() {startPlayer();}, 30000); 
			
			return;
		}
		
		var reportStatusEquipmentOA = {};
		var isReboot = false;
		var isControl = false;
		var equipmentInfo = getStorageData("equipmentInfo");
		if(equipmentInfo) {
			// 장치명 변경
			equipmentInfo.equipmentName = order.equipmentName;
			setStorageData("equipmentInfo", equipmentInfo);
		}
		
		if(order.equipmentUseYn == "N") {//장치 사용 여부 [ N : 해지 , Y: 사용]
			//장치 사용 해제!
			invalidEquipment();
		}
		
		if(order.equipmentMirrorUseYn == "Y") {
			// 미러링시작
			if(MIRRORING_INTERVAL_ID > 0) {
				clearInterval(MIRRORING_INTERVAL_ID);
				MIRRORING_INTERVAL_ID = 0;
			}
			
			doCapture();
			
			MIRRORING_INTERVAL_ID = setInterval(function() {
				doCapture();
			}, DEFAULT_MIRRORING_ONE_CYCLE_INTERVAL);
		}
		
		if(isControlCommand(order.equipmentScreenStatus)) {
			isControl = true;
			if(order.equipmentScreenStatus == "C01") {//장치 화면 상태 [C00 : OFF 명령 , C01 : ON 명령 ]
				DisplayControl("Active");
			} else if(order.equipmentScreenStatus == "C00") {
				DisplayControl("Screen Off");
			}
		}
		
		if (isControlCommand(order.equipmentRealTimeStatus)) {
			// TODO : 로그 업로드 처리
//			if(!bLogFileUploading) {
//				isControl = true;
//				if (order.equipmentRealTimeStatus == "C00") {
//					logger.info("{} : {}", TAG, "로그파일 전송 요청");
//					logger.info("[SD] [로그 요청 시퀀스] 1.1 로그 요청 메시지 전송");
//					mHandler.obtainMessage(1026).sendToTarget();
//				}
//			}
		}
		
//		if (isControlCommand(order.equipmentScreenDirection)) {
//			isControl = true;
//			if (order.equipmentScreenDirection == "C00" || order.equipmentScreenDirection == "C02") {//장치 화면 방향 [ 화면 방향 가로 명령 : C00, 화면 방향 세로 명령 : C01 ]
//				setPortraitMode("off");
//				//sb.append("장치화면방향:가로, ");
//			} else if (order.equipmentScreenDirection == "C01" || order.equipmentScreenDirection == "C03") {
//				setPortraitMode("90");
//				//sb.append("장치화면방향:세로, ");
//			}
//		}
		
		//장치 재시작 여부  C00 : 장치 재시작 명령
		if (isControlCommand(order.equipmentRestartStatus)) {
			isControl = true;
			if (order.equipmentRestartStatus == "C00") {
				//sb.append("장치리부팅, ");

				//장치 재시작 여부 [S00 : 장치 재시작 완료 성공, C00 : 장치 재시작 명령, F00 : 장치 재시작 실패 ]
				reportStatusEquipmentOA.equipmentRestartStatus = "S00";
				//TODO : 리부팅위치를 변경
				isReboot = true;
				//DeviceControlManager.getInstance().reboot();
			} else {
				// ???
			}
		}
		
		if (isControlCommand(order.equipmentVolumeStatus)) {
			isControl = true;
			if (order.equipmentVolumeStatus && order.equipmentVolumeStatus[0] == "C") {
				// 볼륨 상태!
				try {
					var volume = parseInt(order.equipmentVolumeStatus.substring(1), 10);
					setDeviceVolume(volume);
					//sb.append("장치볼륨:" + order.equipmentVolumeStatus + ", ");
				} catch (e) {
					console.error(e.getMessage());
				}
			}
		}
		
		if (isControlCommand(order.screenEquipmentVolumeStatus)) {
			isControl = true;

			if (order.screenEquipmentVolumeStatus && order.screenEquipmentVolumeStatus[0] == "C") {
				// 모니터에 장착된 볼륨 상태!
				try {
					var displayVolume = parseInt(order.screenEquipmentVolumeStatus.substring(1), 10);
					setDeviceVolume(displayVolume);
					//sb.append("모니터볼륨:" + order.screenEquipmentVolumeStatus + ", ");
				} catch (e) {
					console.error(e.getMessage());
				}
			}
		}
		
		if (isControlCommand(order.equipmentBeaconStatus)) {
			isControl = true;
			if (order.equipmentBeaconStatus == "C01") {//장치 BEACON 상태 [ C00 : Off, C01 : On ]
				setBeaconStatus(true);
				//sb.append("Beacon상태:ON");
			} else if (order.equipmentBeaconStatus == "C00") {
				setBeaconStatus(false);
				//sb.append("Beacon상태:OFF");
			}
		}
		
		if (isControlCommand(order.equipmentWifiStatus)) {
			isControl = true;

			if (NETWORK_INFO && NETWORK_INFO.connectType == 1) {
				//off일경우 무조건 세팅
				//on일경우에는 type 1을 체크
//				if (TextUtils.equals(order.equipmentWifiStatus, "C00") || (TextUtils.equals(order.equipmentWifiType, "1") && TextUtils.equals(order.equipmentWifiStatus, "C01"))) {
//					if (TextUtils.isEmpty(order.equipmentWifiSsid) == false && TextUtils.equals(order.equipmentWifiSsid, "null") == false) {//장치 WIFI SSID
	//
//						//암호 설정
//						if (TextUtils.isEmpty(order.equipmentWifiSsidPassword) == false && TextUtils.equals(order.equipmentWifiSsidPassword, "null") == false) {//장치 WIFI SSID 패스워드
	//
//							DeviceControlManager.getInstance().setSoftApSettings(order.equipmentWifiSsid, order.equipmentWifiSsidPassword, false);
//						} else
//							DeviceControlManager.getInstance().setSoftApSettings(order.equipmentWifiSsid, null, false);
	//
//						//장치 WIFI 접속 URL(리다이렉션)
//						if (TextUtils.isEmpty(order.equipmentWifiConnectionUrl) == false && TextUtils.equals(order.equipmentWifiConnectionUrl, "null") == false) {
//							DeviceControlManager.getInstance().setRedirectionPage(order.equipmentWifiConnectionUrl);
//						} else {
//							DeviceControlManager.getInstance().setRedirectionPage("");
//						}
//					} else {
//						DeviceControlManager.getInstance().setSoftApSettings(null, null, false);
//					}
	//
//					//wifi 동작 상태
//					if (TextUtils.equals(order.equipmentWifiStatus, "C01")) {//장치 WIFI 동작 상태 [ C00 : Off,  C01 : On ]
	//
//						//SSID가 없다면 실행시키면 안된다!
//						if (TextUtils.isEmpty(order.equipmentWifiSsid) == false && TextUtils.equals(order.equipmentWifiSsid, "null") == false) {//장치 WIFI SSID
//							// hotspot실행
//							DeviceControlManager.getInstance().enableSoftAP(true);
//						}
//					} else if (TextUtils.equals(order.equipmentWifiStatus, "C00")) {
//						//off 일경우에 무조건 이쪽으로 타게 되어있다 위쪽 if에서 off체크함
//						DeviceControlManager.getInstance().enableSoftAP(false);
//						DeviceControlManager.getInstance().stopSniffing();
//					}
//				} else if (TextUtils.equals(order.equipmentWifiType, "2") && TextUtils.equals(order.equipmentWifiStatus, "C01")) {
//					//센싱이고 켜질때만 체크한다
//					DeviceControlManager.getInstance().startSniffing();
//				}
			} else {
				//WIFI로 네트워크가 연결된 상태에서 hotsopt이나 Sniffing을 작동시키면 안된다 결과는 E99로 전달해준다
				reportStatusEquipmentOA.equipmentWifiStatus = "E99";
			}
		}
		
		if (isControlCommand(order.equipmentScreenOutputStatus)) {
			isControl = true;
			if (order.equipmentScreenOutputStatus == "C00") {//장치 화면 출력 상태 [S00 : 장치 화면 출력 완료 성공, C00 : 장치 화면 출력 명령, F00 : 장치 화면 출력 실패]
				// HSSHIN 모니터 꺼진 경우 켜기
				DisplayControl("Active", function () {
					showToast(order.equipmentName, true);
				});
				reportStatusEquipmentOA.equipmentScreenOutputStatus = "S00";
			}
		}
		
		//장치 하드웨어 수집
		if (isControlCommand(order.equipmentHardwareCollectStatus)) {
			isControl = true;
			if (order.equipmentHardwareCollectStatus == "C00") {//장치 하드웨어 수집 상태 [ C00 : 하드웨어 수집 명령]
				getSystemUsageInfo(function(_cpu, _memory) {
					reportStatusEquipmentOA.equipmentCpuUseVolume = _cpu;
					reportStatusEquipmentOA.equipmentMemoryUseVolume = _memory;
					
					//	HSSHIN 온도 지원 안함 -1 표기
					reportStatusEquipmentOA.equipmentCpuTemperature = "-1";
					reportStatusEquipmentOA.equipmentHardwareCollectStatus = "S00";
					
					getStorageInfo(function(_totalExternal, _totalFreeExternal) {
						reportStatusEquipmentOA.equipmentSdCardTotalVolume = _totalExternal;
						reportStatusEquipmentOA.equipmentSdCardUseVolume = _totalFreeExternal;
						
						if (isControlCommand(order.equipmentScreenDirection)) {
							var mode = (order.equipmentScreenDirection == "C00" || order.equipmentScreenDirection == "C02") ? "off" : "90";
							setPortraitMode(mode, function(_ret) {
								sendReportStatus(order, reportStatusEquipmentOA, isReboot);
							});
						} else {
							sendReportStatus(order, reportStatusEquipmentOA, isReboot);
						}
					});
				});
			}
		} else if(isControlCommand(order.equipmentScreenDirection)) {
			isControl = true;
			var mode = (order.equipmentScreenDirection == "C00" || order.equipmentScreenDirection == "C02") ? "off" : "90";
			setPortraitMode(mode, function() {
				sendReportStatus(order, reportStatusEquipmentOA, isReboot);
			});
		} else {
			if (isControl == false) {
				return true;
			}
			
			sendReportStatus(order, reportStatusEquipmentOA, isReboot);	
		}
	} catch(_Ex) {
		LogManager.error("MessagePull", "EQS201", "<< EQS201 >> Exception : trace =" + _Ex.message);
		
		console.error(_Ex);
	}
}

function sendReportStatus(order, reportStatusEquipmentOA, isReboot) {
	try {
		if (isControlCommand(order.equipmentScreenStatus) || true) {
			if(order.equipmentScreenStatus == "C00") {
				reportStatusEquipmentOA.equipmentScreenStatus = "S00";
			} else {
				reportStatusEquipmentOA.equipmentScreenStatus = "S01";
			}
		}
		
		if (isControlCommand(order.equipmentScreenDirection) || true) {
			var mode = getStorageData("portraitMode");
			reportStatusEquipmentOA.equipmentScreenDirection = "S0" + (mode && mode == "90" ? "1" : "0");
			reportStatusEquipmentOA.equipmentScreenDirection = orderResult(order.equipmentScreenDirection, reportStatusEquipmentOA.equipmentScreenDirection);
		}
		
		if (isControlCommand(order.equipmentVolumeStatus) || true) {
			reportStatusEquipmentOA.equipmentVolumeStatus = "F00";
//			try {
//				int stbVolume = DeviceControlManager.getInstance().getSTBVolume();
	//
//				//장치 볼륨 상태 [S01:볼륨 1, S02:볼륨 2, ... C01:볼륨 1 명령, C02:볼륨 2 명령, ...., F01:볼륨 1 실패, F02:볼륨 2 실패, ...]
//				reportStatusEquipmentOA.equipmentVolumeStatus = String.format("S%02d", stbVolume);
	//
//				reportStatusEquipmentOA.equipmentVolumeStatus = orderResult(order.equipmentVolumeStatus, reportStatusEquipmentOA.equipmentVolumeStatus);
	//
//			} catch (e) {
//				reportStatusEquipmentOA.equipmentVolumeStatus = "F00";
//			}
		}
		
		if (isControlCommand(order.screenEquipmentVolumeStatus) || true) {
			reportStatusEquipmentOA.screenEquipmentVolumeStatus = "E99";
//			if (dpType != DeviceStatusDefine.DISP_TYPE_UNKNOWN) {
//				try {
//					int displayVolume = DeviceControlManager.getInstance().getDpVolume();
	//
//					if(displayVolume < 0)
//						reportStatusEquipmentOA.screenEquipmentVolumeStatus = "F99";
//					else {
//						//장치 볼륨 상태 [S01:볼륨 1, S02:볼륨 2, ... C01:볼륨 1 명령, C02:볼륨 2 명령, ...., F01:볼륨 1 실패, F02:볼륨 2 실패, ...]
//						reportStatusEquipmentOA.screenEquipmentVolumeStatus = String.format("S%02d", displayVolume);
//					}
	//
//					reportStatusEquipmentOA.screenEquipmentVolumeStatus = orderResult(order.screenEquipmentVolumeStatus, reportStatusEquipmentOA.screenEquipmentVolumeStatus);
	//
//				} catch (e) {
//					reportStatusEquipmentOA.screenEquipmentVolumeStatus = "F00";
//				}
//			} else {
//				//지원하지 않는 디스플레이 타입일 경우 E99 반환
//				reportStatusEquipmentOA.screenEquipmentVolumeStatus = "E99";
//			}
		}
		
		//장치 WIFI 동작 상태 [ S00 : OFF 성공 , S01 : ON 성공 , C00 : OFF 명령 , C01 : ON 명령 , F00 : OFF 실패 , F01 : ON 실패 ]
		//WIFI상태의 결과를 취합할때 결과에 E99가 이미 들어가있다면, 취합하지 않고 E99를 그대로 전달해준다
		if ((isControlCommand(order.equipmentWifiStatus) || true) && reportStatusEquipmentOA.equipmentWifiStatus != "E99") {
//			//off명령 일경우 둘다 상태값을 체크한다, 하지만 F00, S00 을 체크해야한다.
//			if (TextUtils.equals(order.equipmentWifiStatus, "C00")) {
//				for (int i = 0; i < 10; i++) {
//					try {
//						Thread.yield();
//						Thread.sleep(1000);
	//
//						//AP먼저 체크
			reportStatusEquipmentOA.equipmentWifiStatus = "S00";
//						reportStatusEquipmentOA.equipmentWifiStatus = (DeviceControlManager.getInstance().getEnableSoftAp()) ? "S01" : "S00";
//						reportStatusEquipmentOA.equipmentWifiStatus = orderResult(order.equipmentWifiStatus, reportStatusEquipmentOA.equipmentWifiStatus);
	//
//						if (TextUtils.equals(reportStatusEquipmentOA.equipmentWifiStatus, "S00")) {
//							//센싱체크
//							reportStatusEquipmentOA.equipmentWifiStatus = (DeviceControlManager.getInstance().isSniffingMode()) ? "S01" : "S00";
//							reportStatusEquipmentOA.equipmentWifiStatus = orderResult(order.equipmentWifiStatus, reportStatusEquipmentOA.equipmentWifiStatus);
//							if (TextUtils.equals(reportStatusEquipmentOA.equipmentWifiStatus, "S00")) {
//								//최정 정상적으로 꺼짐 결과 S00
//								break;
//							}
//						}
	//
//						// 둘중 하나라도 꺼지지 않았을경우 F00으로 전달
//						reportStatusEquipmentOA.equipmentWifiStatus = "F00";
//					} catch (InterruptedException e) {
//						//e.printStackTrace();
//					}
//				} //for
//			} else if (TextUtils.equals(order.equipmentWifiStatus, "C01")) {
//				//on일경우 F01, S01을 체크해야한다.
//				//off명령 일경우 둘다 상태값을 체크한다, 하지만 F00, S00 을 체크해야한다.
//				for (int i = 0; i < 5; i++) {
//					try {
//						Thread.yield();
//						Thread.sleep(1000);
	//
//						if (TextUtils.equals(order.equipmentWifiType, "1")) {
//							//AP먼저 체크
//							reportStatusEquipmentOA.equipmentWifiType = "1";
//							reportStatusEquipmentOA.equipmentWifiStatus = (DeviceControlManager.getInstance().getEnableSoftAp()) ? "S01" : "S00";
//							reportStatusEquipmentOA.equipmentWifiStatus = orderResult(order.equipmentWifiStatus, reportStatusEquipmentOA.equipmentWifiStatus);
//							if (reportStatusEquipmentOA.equipmentWifiStatus.startsWith("S") == true) {
//								break;
//							}
//						} else {
//							//센싱체크
			reportStatusEquipmentOA.equipmentWifiType = "2";
//							reportStatusEquipmentOA.equipmentWifiType = "2";
//							reportStatusEquipmentOA.equipmentWifiStatus = (DeviceControlManager.getInstance().isSniffingMode()) ? "S01" : "S00";
//							reportStatusEquipmentOA.equipmentWifiStatus = orderResult(order.equipmentWifiStatus, reportStatusEquipmentOA.equipmentWifiStatus);
//							if (reportStatusEquipmentOA.equipmentWifiStatus.startsWith("S") == true) {
//								break;
//							}
//						}
//					} catch (InterruptedException e) {
//						//e.printStackTrace();
//					}
//				}
//			} else {
//				//상태값만 조회할경우
//				//두가지 상태 모두 조회해야한다
//				try {
//					//AP먼저 체크
//					reportStatusEquipmentOA.equipmentWifiStatus = (DeviceControlManager.getInstance().getEnableSoftAp()) ? "S01" : "S00";
//					if (TextUtils.equals(reportStatusEquipmentOA.equipmentWifiStatus, "S01")) {
//						reportStatusEquipmentOA.equipmentWifiType = "1";
//					} else {
//						//센싱체크
//						reportStatusEquipmentOA.equipmentWifiStatus = (DeviceControlManager.getInstance().isSniffingMode()) ? "S01" : "S00";
//						if (TextUtils.equals(reportStatusEquipmentOA.equipmentWifiStatus, "S01")) {
//							reportStatusEquipmentOA.equipmentWifiType = "2";
//						}
//					}
//				} catch (Exception e) {
//					//e.printStackTrace();
//				}
//			}
	//
//			//장치 WIFI SSID
//			reportStatusEquipmentOA.equipmentWifiSsid = DeviceControlManager.getInstance().getSoftApSSID();
	//
//			if (TextUtils.isEmpty(reportStatusEquipmentOA.equipmentWifiSsid))
//				reportStatusEquipmentOA.equipmentWifiSsid = "";
	//
//			//장치 WIFI SSID
//			reportStatusEquipmentOA.equipmentWifiSsidPassword = DeviceControlManager.getInstance().getSoftApPassword();
	//
//			if (TextUtils.isEmpty(reportStatusEquipmentOA.equipmentWifiSsidPassword))
//				reportStatusEquipmentOA.equipmentWifiSsidPassword = "";
	//
//			//장치 WIFI 접속 URL
//			reportStatusEquipmentOA.equipmentWifiConnectionUrl = DeviceControlManager.getInstance().getRedirectionPage();
	//
//			if (TextUtils.isEmpty(reportStatusEquipmentOA.equipmentWifiConnectionUrl))
//				reportStatusEquipmentOA.equipmentWifiConnectionUrl = "";
		}
		
		//장치 BEACON 상태 [S00 : OFF 성공 , S01 : ON 성공 , C00 : OFF 명령 , C01 : ON 명령 , F00 : OFF 실패 , F01 : ON 실패]
		if (isControlCommand(order.equipmentBeaconStatus) || true) {
			reportStatusEquipmentOA.equipmentBeaconStatus = "S" + order.equipmentBeaconStatus.substring(1);
			reportStatusEquipmentOA.equipmentBeaconStatus = orderResult(order.equipmentBeaconStatus, reportStatusEquipmentOA.equipmentBeaconStatus);
		}

		//디스플레이 Input 변경 방지 on / off [C00 : OFF 명령 , C01 : ON 명령 ,S00 : OFF 성공 , S01 : ON 성공,F00 : OFF 실패 , F01 : ON 실패]
		if (isControlCommand(order.screenPreventInputChangeStatus) || true) {
			//TODO : 디스플레이 변경 방지가 장치에서 동작하지 않는다..
			reportStatusEquipmentOA.screenPreventInputChangeStatus = "F99";
			reportStatusEquipmentOA.screenPreventInputChangeStatus = orderResult(order.screenPreventInputChangeStatus, reportStatusEquipmentOA.screenPreventInputChangeStatus);
		}
		
		var logString = "";
		
		if (reportStatusEquipmentOA.equipmentScreenStatus) {
			logString += "장치화면상태:" + reportStatusEquipmentOA.equipmentScreenStatus + "\n";
		}

		if (reportStatusEquipmentOA.equipmentScreenDirection) {
			logString += "화면방향:" + reportStatusEquipmentOA.equipmentScreenDirection + "\n";
		}

		if (reportStatusEquipmentOA.equipmentRestartStatus) {
			logString += "장치재시작:" + reportStatusEquipmentOA.equipmentRestartStatus + "\n";
		}

		if (reportStatusEquipmentOA.equipmentVolumeStatus) {
			logString += "장치볼륨:" + reportStatusEquipmentOA.equipmentVolumeStatus + "\n";
		}

		if (reportStatusEquipmentOA.screenEquipmentVolumeStatus) {
			logString += "모니터볼륨:" + reportStatusEquipmentOA.screenEquipmentVolumeStatus + "\n";
		}

		if (reportStatusEquipmentOA.equipmentWifiStatus) {
			logString += "WIFI상태:" + reportStatusEquipmentOA.equipmentWifiStatus + "\n";
		}

		if (reportStatusEquipmentOA.equipmentWifiType) {
			logString += "장치 Wifi 유형:" + reportStatusEquipmentOA.equipmentWifiType + "\n";
		}

		if (reportStatusEquipmentOA.equipmentWifiSsid) {
			logString += "SSID:" + reportStatusEquipmentOA.equipmentWifiSsid + "\n";
		}

		if (reportStatusEquipmentOA.equipmentWifiSsidPassword) {
			logString += "WIFI_PWD:" + reportStatusEquipmentOA.equipmentWifiSsidPassword + "\n";
		}

		if (reportStatusEquipmentOA.equipmentWifiConnectionUrl) {
			logString += "redirection url:" + reportStatusEquipmentOA.equipmentWifiConnectionUrl + "\n";
		}

		if (reportStatusEquipmentOA.equipmentBeaconStatus) {
			logString += "Beacon:" + reportStatusEquipmentOA.equipmentBeaconStatus + "\n";
		}

		if (reportStatusEquipmentOA.equipmentHardwareCollectStatus) {
			logString += "HW수집:" + reportStatusEquipmentOA.equipmentHardwareCollectStatus + "\n";
		}

		if (reportStatusEquipmentOA.equipmentCpuUseVolume) {
			logString += "장치 CPU 사용량:" + reportStatusEquipmentOA.equipmentCpuUseVolume + "\n";
		}

		if (reportStatusEquipmentOA.equipmentCpuTemperature) {
			logString += "장치 CPU 온도:" + reportStatusEquipmentOA.equipmentCpuTemperature + "\n";
		}

		if (reportStatusEquipmentOA.equipmentMemoryUseVolume) {
			logString += "장치 메모리 사용량:" + reportStatusEquipmentOA.equipmentMemoryUseVolume + "\n";
		}

		if (reportStatusEquipmentOA.equipmentSdCardTotalVolume) {
			logString += "장치 SD 카드 전체 용량:" + reportStatusEquipmentOA.equipmentSdCardTotalVolume + "\n";
		}

		if (reportStatusEquipmentOA.equipmentSdCardUseVolume) {
			logString += "장치 SD 카드 사용량:" + reportStatusEquipmentOA.equipmentSdCardUseVolume + "\n";
		}

		if (reportStatusEquipmentOA.equipmentScreenOutputStatus) {
			logString += "장치 화면 출력 상태:" + reportStatusEquipmentOA.equipmentScreenOutputStatus + "\n";
		}

		if (reportStatusEquipmentOA.screenPreventInputChangeStatus) {
			logString += "디스플레이 Input 변경 방지 상태:" + reportStatusEquipmentOA.screenPreventInputChangeStatus + "\n";
		}

		if (isReboot) {
			logString += "리부팅 시작\n";
		}
		
		console.log(logString);
		
		reportStatusEquipment(isReboot, reportStatusEquipmentOA);
	} catch(_Ex) {
		console.error(_Ex);
	}
}

function setDeviceVolume(vol) {
	try {
	    var options = {
	       level : vol,
	       volOsdEnabled : false
	    };
	 
	    function successCb() {
	        // Do something
	    }
	 
	    function failureCb(cbObject) {
	    	LogManager.error("DeviceControlManager", "MID202", "<< MID202 >> invalid volume set =" + vol);
	    	
	        var errorCode = cbObject.errorCode;
	        var errorText = cbObject.errorText;
	 
	        console.log("Error Code [" + errorCode + "]: " + errorText);
	    }
	 
	    var sound = new Sound();
	    sound.setVolumeLevel(successCb, failureCb, options);
	}catch(e) {
		console.log("Error: " + e);
	}
}

function setBeaconStatus(status) {
	
}

function orderResult(command, result) {
	if (isControlCommand(command)) {
		if (command.substring(1) != result.substring(1)) {
			return "F" + result.substring(1);
		}
	}

	return result;
}

function getStorageInfo(_callback) {
	try {
		var successCb = function (cbObject) {
			try {
				var free = cbObject.free;
				var total = cbObject.total;
				var used = cbObject.used;
				var externals = cbObject.externalMemory;
				var totalExternal = 0;
				var totalFreeExternal = 0;
				var totalUsedExternal = 0;
				
				// HSSHIN 2017-12-05 내부 스토리지 용량 조회로 변경함
				/*
				for (var uri in externals) {
					var external = externals[uri];
					
					totalExternal += parseFloat(external.total, 10);
					totalFreeExternal += parseFloat(external.free, 10);
				}
				
				totalExternal = Math.round(totalExternal / 1024);
				totalFreeExternal = Math.round(totalFreeExternal / 1024);
				
				_callback(totalExternal, totalFreeExternal);
			 	*/
				
				totalExternal = Math.round(parseFloat(total, 10) / 1024);
				totalUsedExternal =  Math.round(parseFloat(used, 10) / 1024);
				
				_callback(totalExternal, totalUsedExternal);

			} catch(_Ex) {
				console.error(_Ex);
				
				_callback(0, 0);
			}
		};
		
		var failureCb = function (cbObject) {
			var errorCode = cbObject.errorCode;
			var errorText = cbObject.errorText;
			console.log(" Error Code [" + errorCode + "]: " + errorText);
			
			_callback(0, 0);
		};
		
		var storage = new Storage();
		storage.getStorageInfo(successCb, failureCb);
	} catch(_Ex) {
		console.error(_Ex);
		
		_callback(0, 0);
	}
}

function getSystemUsageInfo(_callback) {
	function successCb(cbObject) {
		try {
			var cpus = cbObject.cpus;
			var memory = cbObject.memory;
			var total = 0;
			var usageTotal = 0;

			for (var i = 0, len = cpus.length; i < len; i++) {
				var cpu = cpus[i];
				for (type in cpu.times) {
					total += cpu.times[type];
				}
			}
			
			for (var i = 0, len = cpus.length; i < len; i++) {
				var cpu = cpus[i];
				for (type in cpu.times) {
					if(type == "idle") {
						continue;
					}

					usageTotal += cpu.times[type];
				}
			}
			
			var cpuUsage = Math.round(100 * usageTotal / total);
			var memoryUsage = Math.round(100 * (memory.total - memory.free) / memory.total);
			
			console.log("CPU: " + cpuUsage + "%");
			console.log("Memory: " + memoryUsage + "%");
			
			_callback(cpuUsage, memoryUsage);
		} catch(_Ex) {
			console.error(_Ex);
			
			_callback(0, 0);
		}
	}
	
	function failureCb(cbObject) {
		var errorCode = cbObject.errorCode;
		var errorText = cbObject.errorText;
		console.log("Error Code [" + errorCode + "]: " + errorText);
		
		_callback(0, 0);
	}
	
	var options = {
		cpus: true,
		memory: true
	};
	
	var deviceInfo = new DeviceInfo();
	deviceInfo.getSystemUsageInfo(successCb, failureCb, options);
}

function checkPortraitMode(_callback) {
	var signage = new Signage();

	var successCb = function (cbObject) {
 
		_callback(cbObject.portraitMode);
    };
 
    var failureCb = function (cbObject) {
    	LogManager.error("DeviceControlManager", "MID201", "<< MID201 >> invalid display mode set=" + mode + ", trace = " + cbObject.errorText);
    	
        var errorCode = cbObject.errorCode;
        var errorText = cbObject.errorText;
        console.log(" Error Code [" + errorCode + "]: " + errorText);
    };
    
    signage.getSignageInfo(successCb, failureCb);
}

/* val : off | 90 */
function setPortraitMode(mode, _callback) {
	checkPortraitMode(function (curr) {
		if(curr != mode) {
		    var options = {
	            portraitMode: mode
	        };
	     
	        var successCb = function () {
	            console.log("Portrait Mode Set : " + mode);
	            setStorageData("portraitMode", mode);
	            
	            if(_callback) {
	            	_callback(true);
	            }
	            
	            changeBodySize();
	            
	            if(CURRENT_PAGE == "singleplayer") {
	            	location.replace("../../views/main.html");
	            }
	            
//	            setContentRotation(mode);
	        };
	     
	        var failureCb = function (cbObject) {
	        	LogManager.error("DeviceControlManager", "MID201", "<< MID201 >> invalid display mode set=" + mode + ", trace = " + cbObject.errorText);
	        	
	            var errorCode = cbObject.errorCode;
	            var errorText = cbObject.errorText;
	            console.log(" Error Code [" + errorCode + "]: " + errorText);

	            if(_callback) {
	            	_callback(false);
	            }
	        };
	     
	        var signage = new Signage();
	        signage.setPortraitMode(successCb, failureCb, options);
		}
		
		changeBodySize();
	});
}

function changeBodySize() {
	try {
		if(CURRENT_PAGE == "main") {
	    	var screenSize = getScreeSize();
	    	if(screenSize) {
				console.log("screen size : " + screenSize.width + " x " + screenSize.height);
			}
	    	
	    	$("body").css({
				width : screenSize.width + "px",
				height: screenSize.height + "px"
			});
	    	
	    	$("footer").css({
	    		width : "100%"
	    	});
	    	
	    	var bodyHeight = $("body").height();
	    	var contentHeight = $("#centerContent").height();
	    	var footerHeight = $("footer").height();
	    	var marginTop = (bodyHeight - contentHeight - footerHeight - 96) / 2;
	    	
	    	console.log("bodyHeight : " + bodyHeight + "px, contentHeight : " + contentHeight + "px, footerHeight : " + footerHeight + "px, marginTop : " + marginTop + "px");
	    	
	    	$("#centerContent").css({
	    		"margin-top" : marginTop + "px"
	    	});
	    	
	    	var modalTop = (bodyHeight - 664) / 2;
	    	
	    	$(".modal").css({
	    		top: modalTop + "px"
	    	});
	    }
	} catch(_Ex) {
		console.error(_Ex);
	}
}

function setContentRotation(mode) {
    var options = {
		degree: mode, aspectRatio : "full"
    };
 
    var successCb = function () {
        console.log("Portrait Mode Set : " + mode);
        
        setStorageData("portraitMode", mode);
    };
 
    var failureCb = function (cbObject) {
        var errorCode = cbObject.errorCode;
        var errorText = cbObject.errorText;
        console.log(" Error Code [" + errorCode + "]: " + errorText);
    };
 
    var video = new Video();
    video.setContentRotation(successCb, failureCb, options);	
}


// 단말 제어 시작

function restartApplication() {
    function successCb() {
        console.log("restart success : ");
    }
 
    function failureCb(cbObject) {
        var errorCode = cbObject.errorCode;
        var errorText = cbObject.errorText;
 
        console.log("Error Code [" + errorCode + "]: " + errorText);
    }
 
    var configuration = new Configuration();
    configuration.restartApplication(successCb, failureCb);
}

/* mode : reboot, powerOff */
function PowerControl(mode) {
	LogManager.error("SignageAgentService", "RBT102", "<< RBT102 >> reboot");
	
    var options = {};
    options.powerCommand = mode;
 
    function successCb() {
        // Do something
    }
 
    function failureCb(cbObject) {
        var errorCode = cbObject.errorCode;
        var errorText = cbObject.errorText;
 
        console.log("Error Code [" + errorCode + "]: " + errorText);
    }
 
    var power = new Power();
    power.executePowerCommand(successCb, failureCb, options);
}

/* mode : Screen Off, Active */
function DisplayControl(mode, _callback) {
    var options = {};
    options.displayMode = mode;
 
    function successCb() {
        // Do something
    	if(_callback) _callback();
    }
 
    function failureCb(cbObject) {
        var errorCode = cbObject.errorCode;
        var errorText = cbObject.errorText;
 
        console.log("Error Code [" + errorCode + "]: " + errorText);
        
        if(_callback) _callback(cbObject);
    }
 
    var power = new Power();
    power.setDisplayMode(successCb, failureCb, options);
}

function doCapture() {
	try {
		if(mirroringCount >= MIRRORING_LIMIT) {
			if(MIRRORING_INTERVAL_ID > 0) {
				clearInterval(MIRRORING_INTERVAL_ID);
				MIRRORING_INTERVAL_ID = 0;
			}
			
			mirroringCount = 0;
			
			return;
		}
		
		var tokenInfo = getStorageData("tokenInfo");
		if(!tokenInfo) {
			return;
		}
		
		mirroringCount++;
		
	    var options = {
	        save : false,
	        thumbnail : false,
	        imgResolution : Signage.ImgResolution.HD
	    };
	 
	    var successCB = function (cbObject) {
	    	try {
	    		var size = cbObject.size;
	            var encoding = cbObject.encoding;
	            var data = cbObject.data;
	     
	            console.log("Got Data size:" + size + ", encoding : " + encoding + ", mirroringCount : " + mirroringCount);
	            
	            var params = { token: tokenInfo.token, mirrorFile : data, equipmentMirrorStopYn: "N", mirroringCount: mirroringCount };
	            if(mirroringCount >= MIRRORING_LIMIT) {
	            	params.equipmentMirrorStopYn = "Y";
	            }
	            	
	            if(MAC_ADDRESS == null){
	        		return;
	        	}
	        	
	    		var request = webOS.service.request("luna://com.lg.app.signage.agentservice/", {
	    	        method:"mirrorEquipment",
	    	        parameters: params,
	    	        onFailure: function(inError) {
	    	        	console.log("doCapture() onFailure - " + JSON.stringify(inError));
	    	        },
	    	        onSuccess: function(inResponse) {  
	    	        	console.log("doCapture() onSuccess - " + JSON.stringify(inResponse));
	    	        },
	    	        subscribe : false,
	    	        resubscribe : false
	    	    });
	    	} catch(_Ex) {
	    		LogManager.error("SignageScreenMirroring", "DSP301", "<< DSP301 >> mirroring fail : " + _Ex.message);
	    		
	    		console.error(_Ex);
	    	}
	    };
	 
	    var failureCB = function (cbObject) {
	    	LogManager.error("SignageScreenMirroring", "DSP301", "<< DSP301 >> mirroring fail, do't get screen");
	    	
	        var errorCode = cbObject.errorCode;
	        var errorText = cbObject.errorText;
	 
	        console.log("Error Code [" + errorCode + "]: " + errorText);
	    }
	 
	    var signage = new Signage();
	    signage.captureScreen(successCB, failureCB, options);
	} catch(_Ex) {
		console.error(_Ex);
	}
}

// 단말 제어 종료

function startSendErrorLog() {
	stopSendErrorLog();
	
	SEND_ERROR_LOG_INTERVAL_ID = setInterval(function() {
		sendErrorLog();
	}, SEND_ERROR_LOG_INTERVAL_DELAY);
}

function sendErrorLog() {
	try {
		if(MAC_ADDRESS == null){
			return;
		}
		
		var tokenInfo = getStorageData("tokenInfo");
		if(!tokenInfo) {
			console.log("sendErrorLog() - tokenInfo is empty.");
			return;
		}
		
		var errorLogList = getStorageData("errorLogList");
		if(!errorLogList || errorLogList.length <= 0) {
			console.log("sendErrorLog() - errorLogList is empty.");
			return;
		}
		
		var sendErrorList = [];
		var sendCount = 100;
		if(errorLogList.length <= 100) {
			sendCount = errorLogList.length;
		}
		
		sendErrorList = errorLogList.splice(0, sendCount);
		
		errorLogList = [];
		
		var params = { token : tokenInfo.token, equipmentLogList: sendErrorList };
		console.log("sendErrorLog() Request - " + JSON.stringify(params));
		
		var request = webOS.service.request("luna://com.lg.app.signage.agentservice/", {
	        method:"logEquipment",
	        parameters: params,
	        onFailure: function(inError) {
	        	console.log("sendErrorLog() onFailure - " + JSON.stringify(inError));
	    		return;
	        },
	        onSuccess: function(inResponse) { 
	        	console.log("sendErrorLog() onSuccess - " + JSON.stringify(inResponse));
	        	
	        	if(inResponse.returnValue === true) {
	        		if(inResponse.retCode == NetworkDefine.RET_CODE_S0000) {
	        			setStorageData("errorLogList", errorLogList);
	        		} else if(inResponse.retCode == NetworkDefine.RET_CODE_E0009 || inResponse.retCode == NetworkDefine.RET_CODE_E1005) {
	    				createToken();
	    			}
	        	}
	        }, onComplete : function() {
				console.log("sendErrorLog() complete");
			},
	        subscribe : false,
	        resubscribe : false
	    });
	} catch(_Ex) {
		console.error(_Ex);
	}
}

function stopSendErrorLog() {
	if(SEND_ERROR_LOG_INTERVAL_ID > 0) {
		clearInterval(SEND_ERROR_LOG_INTERVAL_ID);
		SEND_ERROR_LOG_INTERVAL_ID = 0;
	}
}